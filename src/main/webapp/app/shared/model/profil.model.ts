import { Moment } from 'moment';
import { ISpent } from 'app/shared/model/spent.model';
import { IShoppingCart } from 'app/shared/model/shopping-cart.model';
import { ITask } from 'app/shared/model/task.model';
import { ITaskProject } from 'app/shared/model/task-project.model';
import { ITodoList } from 'app/shared/model/todo-list.model';
import { ITodoListTemplate } from 'app/shared/model/todo-list-template.model';
import { IWallet } from 'app/shared/model/wallet.model';
import { IWorkspace } from 'app/shared/model/workspace.model';

export interface IProfil {
  id?: number;
  displayName?: string;
  lastUpdate?: Moment;
  userLogin?: string;
  userId?: number;
  profilDataId?: number;
  spents?: ISpent[];
  shoppingCarts?: IShoppingCart[];
  tasks?: ITask[];
  taskProjects?: ITaskProject[];
  todoLists?: ITodoList[];
  todoListTemplates?: ITodoListTemplate[];
  wallets?: IWallet[];
  workspaces?: IWorkspace[];
}

export class Profil implements IProfil {
  constructor(
    public id?: number,
    public displayName?: string,
    public lastUpdate?: Moment,
    public userLogin?: string,
    public userId?: number,
    public profilDataId?: number,
    public spents?: ISpent[],
    public shoppingCarts?: IShoppingCart[],
    public tasks?: ITask[],
    public taskProjects?: ITaskProject[],
    public todoLists?: ITodoList[],
    public todoListTemplates?: ITodoListTemplate[],
    public wallets?: IWallet[],
    public workspaces?: IWorkspace[]
  ) {}
}
