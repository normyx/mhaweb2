import { Moment } from 'moment';
import { ISpentSharing } from 'app/shared/model/spent-sharing.model';

export interface ISpent {
  id?: number;
  label?: string;
  description?: string;
  amount?: number;
  spentDate?: Moment;
  confirmed?: boolean;
  lastUpdate?: Moment;
  spentSharings?: ISpentSharing[];
  walletLabel?: string;
  walletId?: number;
  spenderDisplayName?: string;
  spenderId?: number;
}

export class Spent implements ISpent {
  constructor(
    public id?: number,
    public label?: string,
    public description?: string,
    public amount?: number,
    public spentDate?: Moment,
    public confirmed?: boolean,
    public lastUpdate?: Moment,
    public spentSharings?: ISpentSharing[],
    public walletLabel?: string,
    public walletId?: number,
    public spenderDisplayName?: string,
    public spenderId?: number
  ) {
    this.confirmed = this.confirmed || false;
  }
}
