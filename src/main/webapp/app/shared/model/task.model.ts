import { Moment } from 'moment';
import { IProfil } from 'app/shared/model/profil.model';

export interface ITask {
  id?: number;
  label?: string;
  description?: string;
  done?: boolean;
  dueDate?: Moment;
  lastUpdate?: Moment;
  owners?: IProfil[];
  taskProjectLabel?: string;
  taskProjectId?: number;
}

export class Task implements ITask {
  constructor(
    public id?: number,
    public label?: string,
    public description?: string,
    public done?: boolean,
    public dueDate?: Moment,
    public lastUpdate?: Moment,
    public owners?: IProfil[],
    public taskProjectLabel?: string,
    public taskProjectId?: number
  ) {
    this.done = this.done || false;
  }
}
