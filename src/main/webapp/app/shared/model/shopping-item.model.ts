import { Moment } from 'moment';
import { Unit } from 'app/shared/model/enumerations/unit.model';

export interface IShoppingItem {
  id?: number;
  label?: string;
  lastUpdate?: Moment;
  checked?: boolean;
  quantity?: number;
  unit?: Unit;
  shoppingCatalogSheldLabel?: string;
  shoppingCatalogSheldId?: number;
  shoppingCartLabel?: string;
  shoppingCartId?: number;
}

export class ShoppingItem implements IShoppingItem {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: Moment,
    public checked?: boolean,
    public quantity?: number,
    public unit?: Unit,
    public shoppingCatalogSheldLabel?: string,
    public shoppingCatalogSheldId?: number,
    public shoppingCartLabel?: string,
    public shoppingCartId?: number
  ) {
    this.checked = this.checked || false;
  }
}
