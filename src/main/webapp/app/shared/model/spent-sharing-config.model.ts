import { Moment } from 'moment';

export interface ISpentSharingConfig {
  id?: number;
  share?: number;
  lastUpdate?: Moment;
  spentConfigLabel?: string;
  spentConfigId?: number;
  profilDisplayName?: string;
  profilId?: number;
}

export class SpentSharingConfig implements ISpentSharingConfig {
  constructor(
    public id?: number,
    public share?: number,
    public lastUpdate?: Moment,
    public spentConfigLabel?: string,
    public spentConfigId?: number,
    public profilDisplayName?: string,
    public profilId?: number
  ) {}
}
