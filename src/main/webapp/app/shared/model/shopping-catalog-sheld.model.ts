import { Moment } from 'moment';
import { IShoppingCatalogItem } from 'app/shared/model/shopping-catalog-item.model';

export interface IShoppingCatalogSheld {
  id?: number;
  label?: string;
  lastUpdate?: Moment;
  isDefault?: boolean;
  isDeleted?: boolean;
  shoppingCatalogItems?: IShoppingCatalogItem[];
  shoppingCatalogCartLabel?: string;
  shoppingCatalogCartId?: number;
}

export class ShoppingCatalogSheld implements IShoppingCatalogSheld {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: Moment,
    public isDefault?: boolean,
    public isDeleted?: boolean,
    public shoppingCatalogItems?: IShoppingCatalogItem[],
    public shoppingCatalogCartLabel?: string,
    public shoppingCatalogCartId?: number
  ) {
    this.isDefault = this.isDefault || false;
    this.isDeleted = this.isDeleted || false;
  }
}
