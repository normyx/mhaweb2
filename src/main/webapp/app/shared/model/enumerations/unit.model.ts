export const enum Unit {
  QUANTITY = 'QUANTITY',

  G = 'G',

  KG = 'KG',

  L = 'L',

  ML = 'ML',
}
