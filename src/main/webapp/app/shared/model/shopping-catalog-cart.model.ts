import { Moment } from 'moment';
import { IShoppingCatalogSheld } from 'app/shared/model/shopping-catalog-sheld.model';

export interface IShoppingCatalogCart {
  id?: number;
  label?: string;
  lastUpdate?: Moment;
  shoppingCatalogShelds?: IShoppingCatalogSheld[];
}

export class ShoppingCatalogCart implements IShoppingCatalogCart {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: Moment,
    public shoppingCatalogShelds?: IShoppingCatalogSheld[]
  ) {}
}
