import { Moment } from 'moment';
import { ITask } from 'app/shared/model/task.model';
import { IProfil } from 'app/shared/model/profil.model';

export interface ITaskProject {
  id?: number;
  label?: string;
  lastUpdate?: Moment;
  tasks?: ITask[];
  owners?: IProfil[];
  workspaceLabel?: string;
  workspaceId?: number;
}

export class TaskProject implements ITaskProject {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: Moment,
    public tasks?: ITask[],
    public owners?: IProfil[],
    public workspaceLabel?: string,
    public workspaceId?: number
  ) {}
}
