import { Moment } from 'moment';

export interface ISpentSharing {
  id?: number;
  amountShare?: number;
  share?: number;
  lastUpdate?: Moment;
  sharingProfilDisplayName?: string;
  sharingProfilId?: number;
  spentLabel?: string;
  spentId?: number;
}

export class SpentSharing implements ISpentSharing {
  constructor(
    public id?: number,
    public amountShare?: number,
    public share?: number,
    public lastUpdate?: Moment,
    public sharingProfilDisplayName?: string,
    public sharingProfilId?: number,
    public spentLabel?: string,
    public spentId?: number
  ) {}
}
