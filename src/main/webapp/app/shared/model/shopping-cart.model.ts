import { Moment } from 'moment';
import { IShoppingItem } from 'app/shared/model/shopping-item.model';
import { IProfil } from 'app/shared/model/profil.model';

export interface IShoppingCart {
  id?: number;
  label?: string;
  lastUpdate?: Moment;
  shoppingItems?: IShoppingItem[];
  shoppingCatalogCartLabel?: string;
  shoppingCatalogCartId?: number;
  owners?: IProfil[];
  workspaceLabel?: string;
  workspaceId?: number;
}

export class ShoppingCart implements IShoppingCart {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: Moment,
    public shoppingItems?: IShoppingItem[],
    public shoppingCatalogCartLabel?: string,
    public shoppingCatalogCartId?: number,
    public owners?: IProfil[],
    public workspaceLabel?: string,
    public workspaceId?: number
  ) {}
}
