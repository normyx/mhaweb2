import { Component } from '@angular/core';

@Component({
  selector: 'mha-docs',
  templateUrl: './docs.component.html',
  styleUrls: ['docs.scss'],
})
export class DocsComponent {}
