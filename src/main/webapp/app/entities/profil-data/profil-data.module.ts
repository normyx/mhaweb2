import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Mhaweb2SharedModule } from 'app/shared/shared.module';
import { ProfilDataComponent } from './profil-data.component';
import { ProfilDataDetailComponent } from './profil-data-detail.component';
import { ProfilDataUpdateComponent } from './profil-data-update.component';
import { ProfilDataDeleteDialogComponent } from './profil-data-delete-dialog.component';
import { profilDataRoute } from './profil-data.route';

@NgModule({
  imports: [Mhaweb2SharedModule, RouterModule.forChild(profilDataRoute)],
  declarations: [ProfilDataComponent, ProfilDataDetailComponent, ProfilDataUpdateComponent, ProfilDataDeleteDialogComponent],
  entryComponents: [ProfilDataDeleteDialogComponent],
})
export class Mhaweb2ProfilDataModule {}
