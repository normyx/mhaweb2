import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Mhaweb2SharedModule } from 'app/shared/shared.module';
import { ShoppingItemComponent } from './shopping-item.component';
import { ShoppingItemDetailComponent } from './shopping-item-detail.component';
import { ShoppingItemUpdateComponent } from './shopping-item-update.component';
import { ShoppingItemDeleteDialogComponent } from './shopping-item-delete-dialog.component';
import { shoppingItemRoute } from './shopping-item.route';

@NgModule({
  imports: [Mhaweb2SharedModule, RouterModule.forChild(shoppingItemRoute)],
  declarations: [ShoppingItemComponent, ShoppingItemDetailComponent, ShoppingItemUpdateComponent, ShoppingItemDeleteDialogComponent],
  entryComponents: [ShoppingItemDeleteDialogComponent],
})
export class Mhaweb2ShoppingItemModule {}
