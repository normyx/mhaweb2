import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'profil',
        loadChildren: () => import('./profil/profil.module').then(m => m.Mhaweb2ProfilModule),
      },
      {
        path: 'profil-data',
        loadChildren: () => import('./profil-data/profil-data.module').then(m => m.Mhaweb2ProfilDataModule),
      },
      {
        path: 'shopping-cart',
        loadChildren: () => import('./shopping-cart/shopping-cart.module').then(m => m.Mhaweb2ShoppingCartModule),
      },
      {
        path: 'shopping-catalog-cart',
        loadChildren: () => import('./shopping-catalog-cart/shopping-catalog-cart.module').then(m => m.Mhaweb2ShoppingCatalogCartModule),
      },
      {
        path: 'shopping-catalog-item',
        loadChildren: () => import('./shopping-catalog-item/shopping-catalog-item.module').then(m => m.Mhaweb2ShoppingCatalogItemModule),
      },
      {
        path: 'shopping-catalog-sheld',
        loadChildren: () => import('./shopping-catalog-sheld/shopping-catalog-sheld.module').then(m => m.Mhaweb2ShoppingCatalogSheldModule),
      },
      {
        path: 'shopping-item',
        loadChildren: () => import('./shopping-item/shopping-item.module').then(m => m.Mhaweb2ShoppingItemModule),
      },
      {
        path: 'spent',
        loadChildren: () => import('./spent/spent.module').then(m => m.Mhaweb2SpentModule),
      },
      {
        path: 'spent-config',
        loadChildren: () => import('./spent-config/spent-config.module').then(m => m.Mhaweb2SpentConfigModule),
      },
      {
        path: 'spent-sharing',
        loadChildren: () => import('./spent-sharing/spent-sharing.module').then(m => m.Mhaweb2SpentSharingModule),
      },
      {
        path: 'spent-sharing-config',
        loadChildren: () => import('./spent-sharing-config/spent-sharing-config.module').then(m => m.Mhaweb2SpentSharingConfigModule),
      },
      {
        path: 'task',
        loadChildren: () => import('./task/task.module').then(m => m.Mhaweb2TaskModule),
      },
      {
        path: 'task-project',
        loadChildren: () => import('./task-project/task-project.module').then(m => m.Mhaweb2TaskProjectModule),
      },
      {
        path: 'todo',
        loadChildren: () => import('./todo/todo.module').then(m => m.Mhaweb2TodoModule),
      },
      {
        path: 'todo-list',
        loadChildren: () => import('./todo-list/todo-list.module').then(m => m.Mhaweb2TodoListModule),
      },
      {
        path: 'todo-list-template',
        loadChildren: () => import('./todo-list-template/todo-list-template.module').then(m => m.Mhaweb2TodoListTemplateModule),
      },
      {
        path: 'todo-template',
        loadChildren: () => import('./todo-template/todo-template.module').then(m => m.Mhaweb2TodoTemplateModule),
      },
      {
        path: 'wallet',
        loadChildren: () => import('./wallet/wallet.module').then(m => m.Mhaweb2WalletModule),
      },
      {
        path: 'workspace',
        loadChildren: () => import('./workspace/workspace.module').then(m => m.Mhaweb2WorkspaceModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class Mhaweb2EntityModule {}
