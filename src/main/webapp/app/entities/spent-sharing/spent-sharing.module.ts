import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Mhaweb2SharedModule } from 'app/shared/shared.module';
import { SpentSharingComponent } from './spent-sharing.component';
import { SpentSharingDetailComponent } from './spent-sharing-detail.component';
import { SpentSharingUpdateComponent } from './spent-sharing-update.component';
import { SpentSharingDeleteDialogComponent } from './spent-sharing-delete-dialog.component';
import { spentSharingRoute } from './spent-sharing.route';

@NgModule({
  imports: [Mhaweb2SharedModule, RouterModule.forChild(spentSharingRoute)],
  declarations: [SpentSharingComponent, SpentSharingDetailComponent, SpentSharingUpdateComponent, SpentSharingDeleteDialogComponent],
  entryComponents: [SpentSharingDeleteDialogComponent],
})
export class Mhaweb2SpentSharingModule {}
