package com.mgoulene.mhaweb.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.mhaweb.domain.Profil;
import com.mgoulene.mhaweb.domain.*; // for static metamodels
import com.mgoulene.mhaweb.repository.ProfilRepository;
import com.mgoulene.mhaweb.service.dto.ProfilCriteria;
import com.mgoulene.mhaweb.service.dto.ProfilDTO;
import com.mgoulene.mhaweb.service.mapper.ProfilMapper;

/**
 * Service for executing complex queries for {@link Profil} entities in the database.
 * The main input is a {@link ProfilCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProfilDTO} or a {@link Page} of {@link ProfilDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProfilQueryService extends QueryService<Profil> {

    private final Logger log = LoggerFactory.getLogger(ProfilQueryService.class);

    private final ProfilRepository profilRepository;

    private final ProfilMapper profilMapper;

    public ProfilQueryService(ProfilRepository profilRepository, ProfilMapper profilMapper) {
        this.profilRepository = profilRepository;
        this.profilMapper = profilMapper;
    }

    /**
     * Return a {@link List} of {@link ProfilDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProfilDTO> findByCriteria(ProfilCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Profil> specification = createSpecification(criteria);
        return profilMapper.toDto(profilRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProfilDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProfilDTO> findByCriteria(ProfilCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Profil> specification = createSpecification(criteria);
        return profilRepository.findAll(specification, page)
            .map(profilMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProfilCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Profil> specification = createSpecification(criteria);
        return profilRepository.count(specification);
    }

    /**
     * Function to convert {@link ProfilCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Profil> createSpecification(ProfilCriteria criteria) {
        Specification<Profil> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Profil_.id));
            }
            if (criteria.getDisplayName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDisplayName(), Profil_.displayName));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), Profil_.lastUpdate));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Profil_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getProfilDataId() != null) {
                specification = specification.and(buildSpecification(criteria.getProfilDataId(),
                    root -> root.join(Profil_.profilData, JoinType.LEFT).get(ProfilData_.id)));
            }
            if (criteria.getSpentId() != null) {
                specification = specification.and(buildSpecification(criteria.getSpentId(),
                    root -> root.join(Profil_.spents, JoinType.LEFT).get(Spent_.id)));
            }
            if (criteria.getShoppingCartId() != null) {
                specification = specification.and(buildSpecification(criteria.getShoppingCartId(),
                    root -> root.join(Profil_.shoppingCarts, JoinType.LEFT).get(ShoppingCart_.id)));
            }
            if (criteria.getTaskId() != null) {
                specification = specification.and(buildSpecification(criteria.getTaskId(),
                    root -> root.join(Profil_.tasks, JoinType.LEFT).get(Task_.id)));
            }
            if (criteria.getTaskProjectId() != null) {
                specification = specification.and(buildSpecification(criteria.getTaskProjectId(),
                    root -> root.join(Profil_.taskProjects, JoinType.LEFT).get(TaskProject_.id)));
            }
            if (criteria.getTodoListId() != null) {
                specification = specification.and(buildSpecification(criteria.getTodoListId(),
                    root -> root.join(Profil_.todoLists, JoinType.LEFT).get(TodoList_.id)));
            }
            if (criteria.getTodoListTemplateId() != null) {
                specification = specification.and(buildSpecification(criteria.getTodoListTemplateId(),
                    root -> root.join(Profil_.todoListTemplates, JoinType.LEFT).get(TodoListTemplate_.id)));
            }
            if (criteria.getWalletId() != null) {
                specification = specification.and(buildSpecification(criteria.getWalletId(),
                    root -> root.join(Profil_.wallets, JoinType.LEFT).get(Wallet_.id)));
            }
            if (criteria.getWorkspaceId() != null) {
                specification = specification.and(buildSpecification(criteria.getWorkspaceId(),
                    root -> root.join(Profil_.workspaces, JoinType.LEFT).get(Workspace_.id)));
            }
        }
        return specification;
    }
}
