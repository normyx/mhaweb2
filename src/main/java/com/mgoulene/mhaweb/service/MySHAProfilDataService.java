package com.mgoulene.mhaweb.service;

import java.time.LocalDate;
import java.util.List;

import com.mgoulene.mhaweb.domain.ProfilData;
import com.mgoulene.mhaweb.domain.TaskProject;
import com.mgoulene.mhaweb.repository.MySHAProfilDataRepository;
import com.mgoulene.mhaweb.repository.ProfilDataRepository;
import com.mgoulene.mhaweb.service.dto.ProfilDataDTO;
import com.mgoulene.mhaweb.service.mapper.ProfilDataMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link TaskProject}.
 */
@Service
@Transactional
public class MySHAProfilDataService extends ProfilDataService {

    private final Logger log = LoggerFactory.getLogger(MySHAProfilDataService.class);

    private final MySHAProfilDataRepository mySHAProfilDataRepository;


    public MySHAProfilDataService(ProfilDataRepository profilDataRepository, ProfilDataMapper profilDataMapper,
    MySHAProfilDataRepository mySHAProfilDataRepository) {
        super(profilDataRepository, profilDataMapper);
        this.mySHAProfilDataRepository = mySHAProfilDataRepository;
    }

    /**
     * Get all the taskProjects owned by a given owner.
     *
     * @param ownerId the id of the owner
     * @return the list of entities.
     */
    public List<ProfilDataDTO> findAllWhereWorkspace(List<Long> workspaceIds, LocalDate refreshDate) {
        List<ProfilData> profilDatas= refreshDate != null ? mySHAProfilDataRepository.findAllWhereWorkspace(workspaceIds, refreshDate):mySHAProfilDataRepository.findAllWhereWorkspace(workspaceIds);
        return profilDataMapper.toDto(profilDatas);
    }

   
}
