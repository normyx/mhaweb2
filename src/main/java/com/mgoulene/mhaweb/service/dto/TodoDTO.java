package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.Todo} entity.
 */
public class TodoDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    private String label;

    @NotNull
    private Boolean done;

    @NotNull
    private LocalDate lastUpdate;


    private Long todoListId;

    private String todoListLabel;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Boolean isDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Long getTodoListId() {
        return todoListId;
    }

    public void setTodoListId(Long todoListId) {
        this.todoListId = todoListId;
    }

    public String getTodoListLabel() {
        return todoListLabel;
    }

    public void setTodoListLabel(String todoListLabel) {
        this.todoListLabel = todoListLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TodoDTO)) {
            return false;
        }

        return id != null && id.equals(((TodoDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TodoDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", done='" + isDone() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", todoListId=" + getTodoListId() +
            ", todoListLabel='" + getTodoListLabel() + "'" +
            "}";
    }
}
