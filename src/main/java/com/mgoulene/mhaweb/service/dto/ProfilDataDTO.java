package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.ProfilData} entity.
 */
public class ProfilDataDTO implements Serializable {
    
    private Long id;

    
    @Lob
    private byte[] photo;

    private String photoContentType;
    @NotNull
    private LocalDate lastUpdate;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProfilDataDTO)) {
            return false;
        }

        return id != null && id.equals(((ProfilDataDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProfilDataDTO{" +
            "id=" + getId() +
            ", photo='" + getPhoto() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
