package com.mgoulene.mhaweb.service.dto;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link com.mgoulene.mhaweb.domain.TodoList} entity.
 */
public class TodoListDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(min = 5, max = 40)
    private String label;

    @NotNull
    private LocalDate lastUpdate;

    private Set<ProfilDTO> owners = new HashSet<>();

    private Long workspaceId;

    private String workspaceLabel;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Set<ProfilDTO> getOwners() {
        return owners;
    }

    public void setOwners(Set<ProfilDTO> profils) {
        this.owners = profils;
    }

    public Long getWorkspaceId() {
        return workspaceId;
    }

    public void setWorkspaceId(Long workspaceId) {
        this.workspaceId = workspaceId;
    }

    public String getWorkspaceLabel() {
        return workspaceLabel;
    }

    public void setWorkspaceLabel(String workspaceLabel) {
        this.workspaceLabel = workspaceLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TodoListDTO)) {
            return false;
        }

        return id != null && id.equals(((TodoListDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TodoListDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", owners='" + getOwners() + "'" +
            ", workspaceId=" + getWorkspaceId() +
            ", workspaceLabel='" + getWorkspaceLabel() + "'" +
            "}";
    }
}
