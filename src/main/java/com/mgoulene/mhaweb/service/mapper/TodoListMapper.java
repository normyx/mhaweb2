package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.TodoListDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TodoList} and its DTO {@link TodoListDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProfilMapper.class, WorkspaceMapper.class})
public interface TodoListMapper extends EntityMapper<TodoListDTO, TodoList> {

    @Mapping(source = "workspace.id", target = "workspaceId")
    @Mapping(source = "workspace.label", target = "workspaceLabel")
    TodoListDTO toDto(TodoList todoList);

    @Mapping(target = "todos", ignore = true)
    @Mapping(target = "removeTodo", ignore = true)
    @Mapping(target = "removeOwner", ignore = true)
    @Mapping(source = "workspaceId", target = "workspace")
    TodoList toEntity(TodoListDTO todoListDTO);

    default TodoList fromId(Long id) {
        if (id == null) {
            return null;
        }
        TodoList todoList = new TodoList();
        todoList.setId(id);
        return todoList;
    }
}
