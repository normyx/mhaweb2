package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogSheldDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShoppingCatalogSheld} and its DTO {@link ShoppingCatalogSheldDTO}.
 */
@Mapper(componentModel = "spring", uses = {ShoppingCatalogCartMapper.class})
public interface ShoppingCatalogSheldMapper extends EntityMapper<ShoppingCatalogSheldDTO, ShoppingCatalogSheld> {

    @Mapping(source = "shoppingCatalogCart.id", target = "shoppingCatalogCartId")
    @Mapping(source = "shoppingCatalogCart.label", target = "shoppingCatalogCartLabel")
    ShoppingCatalogSheldDTO toDto(ShoppingCatalogSheld shoppingCatalogSheld);

    @Mapping(target = "shoppingCatalogItems", ignore = true)
    @Mapping(target = "removeShoppingCatalogItem", ignore = true)
    @Mapping(source = "shoppingCatalogCartId", target = "shoppingCatalogCart")
    ShoppingCatalogSheld toEntity(ShoppingCatalogSheldDTO shoppingCatalogSheldDTO);

    default ShoppingCatalogSheld fromId(Long id) {
        if (id == null) {
            return null;
        }
        ShoppingCatalogSheld shoppingCatalogSheld = new ShoppingCatalogSheld();
        shoppingCatalogSheld.setId(id);
        return shoppingCatalogSheld;
    }
}
