package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.SpentConfigDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link SpentConfig} and its DTO {@link SpentConfigDTO}.
 */
@Mapper(componentModel = "spring", uses = {WalletMapper.class})
public interface SpentConfigMapper extends EntityMapper<SpentConfigDTO, SpentConfig> {

    @Mapping(source = "wallet.id", target = "walletId")
    @Mapping(source = "wallet.label", target = "walletLabel")
    SpentConfigDTO toDto(SpentConfig spentConfig);

    @Mapping(source = "walletId", target = "wallet")
    SpentConfig toEntity(SpentConfigDTO spentConfigDTO);

    default SpentConfig fromId(Long id) {
        if (id == null) {
            return null;
        }
        SpentConfig spentConfig = new SpentConfig();
        spentConfig.setId(id);
        return spentConfig;
    }
}
