package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.TodoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Todo} and its DTO {@link TodoDTO}.
 */
@Mapper(componentModel = "spring", uses = {TodoListMapper.class})
public interface TodoMapper extends EntityMapper<TodoDTO, Todo> {

    @Mapping(source = "todoList.id", target = "todoListId")
    @Mapping(source = "todoList.label", target = "todoListLabel")
    TodoDTO toDto(Todo todo);

    @Mapping(source = "todoListId", target = "todoList")
    Todo toEntity(TodoDTO todoDTO);

    default Todo fromId(Long id) {
        if (id == null) {
            return null;
        }
        Todo todo = new Todo();
        todo.setId(id);
        return todo;
    }
}
