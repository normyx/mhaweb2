package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.ProfilDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Profil} and its DTO {@link ProfilDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, ProfilDataMapper.class})
public interface ProfilMapper extends EntityMapper<ProfilDTO, Profil> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "profilData.id", target = "profilDataId")
    ProfilDTO toDto(Profil profil);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "profilDataId", target = "profilData")
    @Mapping(target = "spents", ignore = true)
    @Mapping(target = "removeSpent", ignore = true)
    @Mapping(target = "shoppingCarts", ignore = true)
    @Mapping(target = "removeShoppingCart", ignore = true)
    @Mapping(target = "tasks", ignore = true)
    @Mapping(target = "removeTask", ignore = true)
    @Mapping(target = "taskProjects", ignore = true)
    @Mapping(target = "removeTaskProject", ignore = true)
    @Mapping(target = "todoLists", ignore = true)
    @Mapping(target = "removeTodoList", ignore = true)
    @Mapping(target = "todoListTemplates", ignore = true)
    @Mapping(target = "removeTodoListTemplate", ignore = true)
    @Mapping(target = "wallets", ignore = true)
    @Mapping(target = "removeWallet", ignore = true)
    @Mapping(target = "workspaces", ignore = true)
    @Mapping(target = "removeWorkspace", ignore = true)
    Profil toEntity(ProfilDTO profilDTO);

    default Profil fromId(Long id) {
        if (id == null) {
            return null;
        }
        Profil profil = new Profil();
        profil.setId(id);
        return profil;
    }
}
