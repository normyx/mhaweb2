package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.TaskDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Task} and its DTO {@link TaskDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProfilMapper.class, TaskProjectMapper.class})
public interface TaskMapper extends EntityMapper<TaskDTO, Task> {

    @Mapping(source = "taskProject.id", target = "taskProjectId")
    @Mapping(source = "taskProject.label", target = "taskProjectLabel")
    TaskDTO toDto(Task task);

    @Mapping(target = "removeOwner", ignore = true)
    @Mapping(source = "taskProjectId", target = "taskProject")
    Task toEntity(TaskDTO taskDTO);

    default Task fromId(Long id) {
        if (id == null) {
            return null;
        }
        Task task = new Task();
        task.setId(id);
        return task;
    }
}
