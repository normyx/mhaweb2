package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.TaskProjectDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TaskProject} and its DTO {@link TaskProjectDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProfilMapper.class, WorkspaceMapper.class})
public interface TaskProjectMapper extends EntityMapper<TaskProjectDTO, TaskProject> {

    @Mapping(source = "workspace.id", target = "workspaceId")
    @Mapping(source = "workspace.label", target = "workspaceLabel")
    TaskProjectDTO toDto(TaskProject taskProject);

    @Mapping(target = "tasks", ignore = true)
    @Mapping(target = "removeTask", ignore = true)
    @Mapping(target = "removeOwner", ignore = true)
    @Mapping(source = "workspaceId", target = "workspace")
    TaskProject toEntity(TaskProjectDTO taskProjectDTO);

    default TaskProject fromId(Long id) {
        if (id == null) {
            return null;
        }
        TaskProject taskProject = new TaskProject();
        taskProject.setId(id);
        return taskProject;
    }
}
