package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.TodoListTemplateDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TodoListTemplate} and its DTO {@link TodoListTemplateDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProfilMapper.class, WorkspaceMapper.class})
public interface TodoListTemplateMapper extends EntityMapper<TodoListTemplateDTO, TodoListTemplate> {

    @Mapping(source = "workspace.id", target = "workspaceId")
    @Mapping(source = "workspace.label", target = "workspaceLabel")
    TodoListTemplateDTO toDto(TodoListTemplate todoListTemplate);

    @Mapping(target = "todoTemplates", ignore = true)
    @Mapping(target = "removeTodoTemplate", ignore = true)
    @Mapping(target = "removeOwner", ignore = true)
    @Mapping(source = "workspaceId", target = "workspace")
    TodoListTemplate toEntity(TodoListTemplateDTO todoListTemplateDTO);

    default TodoListTemplate fromId(Long id) {
        if (id == null) {
            return null;
        }
        TodoListTemplate todoListTemplate = new TodoListTemplate();
        todoListTemplate.setId(id);
        return todoListTemplate;
    }
}
