package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.WorkspaceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Workspace} and its DTO {@link WorkspaceDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProfilMapper.class})
public interface WorkspaceMapper extends EntityMapper<WorkspaceDTO, Workspace> {


    @Mapping(target = "shoppingCarts", ignore = true)
    @Mapping(target = "removeShoppingCart", ignore = true)
    @Mapping(target = "taskProjects", ignore = true)
    @Mapping(target = "removeTaskProject", ignore = true)
    @Mapping(target = "todoLists", ignore = true)
    @Mapping(target = "removeTodoList", ignore = true)
    @Mapping(target = "todoListTemplates", ignore = true)
    @Mapping(target = "removeTodoListTemplate", ignore = true)
    @Mapping(target = "wallets", ignore = true)
    @Mapping(target = "removeWallet", ignore = true)
    @Mapping(target = "removeOwner", ignore = true)
    Workspace toEntity(WorkspaceDTO workspaceDTO);

    default Workspace fromId(Long id) {
        if (id == null) {
            return null;
        }
        Workspace workspace = new Workspace();
        workspace.setId(id);
        return workspace;
    }
}
