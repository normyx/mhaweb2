package com.mgoulene.mhaweb.service.mapper;


import com.mgoulene.mhaweb.domain.*;
import com.mgoulene.mhaweb.service.dto.TodoTemplateDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TodoTemplate} and its DTO {@link TodoTemplateDTO}.
 */
@Mapper(componentModel = "spring", uses = {TodoListTemplateMapper.class})
public interface TodoTemplateMapper extends EntityMapper<TodoTemplateDTO, TodoTemplate> {

    @Mapping(source = "todoListTemplate.id", target = "todoListTemplateId")
    @Mapping(source = "todoListTemplate.label", target = "todoListTemplateLabel")
    TodoTemplateDTO toDto(TodoTemplate todoTemplate);

    @Mapping(source = "todoListTemplateId", target = "todoListTemplate")
    TodoTemplate toEntity(TodoTemplateDTO todoTemplateDTO);

    default TodoTemplate fromId(Long id) {
        if (id == null) {
            return null;
        }
        TodoTemplate todoTemplate = new TodoTemplate();
        todoTemplate.setId(id);
        return todoTemplate;
    }
}
