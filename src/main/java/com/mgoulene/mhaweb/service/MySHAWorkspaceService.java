package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.TaskProject;
import com.mgoulene.mhaweb.repository.MySHAWorkspaceRepository;
import com.mgoulene.mhaweb.repository.WorkspaceRepository;
import com.mgoulene.mhaweb.service.dto.WorkspaceDTO;
import com.mgoulene.mhaweb.service.mapper.WorkspaceMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link TaskProject}.
 */
@Service
@Transactional
public class MySHAWorkspaceService extends WorkspaceService {

    private final Logger log = LoggerFactory.getLogger(MySHAWorkspaceService.class);

    private final MySHAWorkspaceRepository mySHAWorkspaceRepository;


    public MySHAWorkspaceService(WorkspaceRepository workspaceRepository, WorkspaceMapper workspaceMapper, MySHAWorkspaceRepository mySHAWorkspaceRepository) {
        super(workspaceRepository, workspaceMapper);
        this.mySHAWorkspaceRepository = mySHAWorkspaceRepository;
    }

    /**
     * Get all the taskProjects owned by a given owner.
     *
     * @param ownerId the id of the owner
     * @return the list of entities.
     */
    public Page<WorkspaceDTO> findAllWhereOwnerId(Pageable pageable, Long ownerId) {
        return mySHAWorkspaceRepository.findAllWhereOwnerId(pageable, ownerId).map(workspaceMapper::toDto);
    }
}
