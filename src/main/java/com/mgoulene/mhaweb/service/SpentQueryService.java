package com.mgoulene.mhaweb.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.mhaweb.domain.Spent;
import com.mgoulene.mhaweb.domain.*; // for static metamodels
import com.mgoulene.mhaweb.repository.SpentRepository;
import com.mgoulene.mhaweb.service.dto.SpentCriteria;
import com.mgoulene.mhaweb.service.dto.SpentDTO;
import com.mgoulene.mhaweb.service.mapper.SpentMapper;

/**
 * Service for executing complex queries for {@link Spent} entities in the database.
 * The main input is a {@link SpentCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SpentDTO} or a {@link Page} of {@link SpentDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SpentQueryService extends QueryService<Spent> {

    private final Logger log = LoggerFactory.getLogger(SpentQueryService.class);

    private final SpentRepository spentRepository;

    private final SpentMapper spentMapper;

    public SpentQueryService(SpentRepository spentRepository, SpentMapper spentMapper) {
        this.spentRepository = spentRepository;
        this.spentMapper = spentMapper;
    }

    /**
     * Return a {@link List} of {@link SpentDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SpentDTO> findByCriteria(SpentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Spent> specification = createSpecification(criteria);
        return spentMapper.toDto(spentRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SpentDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SpentDTO> findByCriteria(SpentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Spent> specification = createSpecification(criteria);
        return spentRepository.findAll(specification, page)
            .map(spentMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SpentCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Spent> specification = createSpecification(criteria);
        return spentRepository.count(specification);
    }

    /**
     * Function to convert {@link SpentCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Spent> createSpecification(SpentCriteria criteria) {
        Specification<Spent> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Spent_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), Spent_.label));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Spent_.description));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), Spent_.amount));
            }
            if (criteria.getSpentDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSpentDate(), Spent_.spentDate));
            }
            if (criteria.getConfirmed() != null) {
                specification = specification.and(buildSpecification(criteria.getConfirmed(), Spent_.confirmed));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), Spent_.lastUpdate));
            }
            if (criteria.getSpentSharingId() != null) {
                specification = specification.and(buildSpecification(criteria.getSpentSharingId(),
                    root -> root.join(Spent_.spentSharings, JoinType.LEFT).get(SpentSharing_.id)));
            }
            if (criteria.getWalletId() != null) {
                specification = specification.and(buildSpecification(criteria.getWalletId(),
                    root -> root.join(Spent_.wallet, JoinType.LEFT).get(Wallet_.id)));
            }
            if (criteria.getSpenderId() != null) {
                specification = specification.and(buildSpecification(criteria.getSpenderId(),
                    root -> root.join(Spent_.spender, JoinType.LEFT).get(Profil_.id)));
            }
        }
        return specification;
    }
}
