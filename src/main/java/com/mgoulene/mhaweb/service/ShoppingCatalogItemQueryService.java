package com.mgoulene.mhaweb.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.mhaweb.domain.ShoppingCatalogItem;
import com.mgoulene.mhaweb.domain.*; // for static metamodels
import com.mgoulene.mhaweb.repository.ShoppingCatalogItemRepository;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogItemCriteria;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogItemDTO;
import com.mgoulene.mhaweb.service.mapper.ShoppingCatalogItemMapper;

/**
 * Service for executing complex queries for {@link ShoppingCatalogItem} entities in the database.
 * The main input is a {@link ShoppingCatalogItemCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ShoppingCatalogItemDTO} or a {@link Page} of {@link ShoppingCatalogItemDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ShoppingCatalogItemQueryService extends QueryService<ShoppingCatalogItem> {

    private final Logger log = LoggerFactory.getLogger(ShoppingCatalogItemQueryService.class);

    private final ShoppingCatalogItemRepository shoppingCatalogItemRepository;

    private final ShoppingCatalogItemMapper shoppingCatalogItemMapper;

    public ShoppingCatalogItemQueryService(ShoppingCatalogItemRepository shoppingCatalogItemRepository, ShoppingCatalogItemMapper shoppingCatalogItemMapper) {
        this.shoppingCatalogItemRepository = shoppingCatalogItemRepository;
        this.shoppingCatalogItemMapper = shoppingCatalogItemMapper;
    }

    /**
     * Return a {@link List} of {@link ShoppingCatalogItemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ShoppingCatalogItemDTO> findByCriteria(ShoppingCatalogItemCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ShoppingCatalogItem> specification = createSpecification(criteria);
        return shoppingCatalogItemMapper.toDto(shoppingCatalogItemRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ShoppingCatalogItemDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ShoppingCatalogItemDTO> findByCriteria(ShoppingCatalogItemCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ShoppingCatalogItem> specification = createSpecification(criteria);
        return shoppingCatalogItemRepository.findAll(specification, page)
            .map(shoppingCatalogItemMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ShoppingCatalogItemCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ShoppingCatalogItem> specification = createSpecification(criteria);
        return shoppingCatalogItemRepository.count(specification);
    }

    /**
     * Function to convert {@link ShoppingCatalogItemCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ShoppingCatalogItem> createSpecification(ShoppingCatalogItemCriteria criteria) {
        Specification<ShoppingCatalogItem> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ShoppingCatalogItem_.id));
            }
            if (criteria.getLabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabel(), ShoppingCatalogItem_.label));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), ShoppingCatalogItem_.lastUpdate));
            }
            if (criteria.getShoppingCatalogSheldId() != null) {
                specification = specification.and(buildSpecification(criteria.getShoppingCatalogSheldId(),
                    root -> root.join(ShoppingCatalogItem_.shoppingCatalogSheld, JoinType.LEFT).get(ShoppingCatalogSheld_.id)));
            }
        }
        return specification;
    }
}
