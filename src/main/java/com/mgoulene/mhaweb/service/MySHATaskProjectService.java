package com.mgoulene.mhaweb.service;

import com.mgoulene.mhaweb.domain.TaskProject;
import com.mgoulene.mhaweb.repository.MySHATaskProjectRepository;
import com.mgoulene.mhaweb.repository.TaskProjectRepository;
import com.mgoulene.mhaweb.service.dto.TaskProjectDTO;
import com.mgoulene.mhaweb.service.mapper.TaskProjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link TaskProject}.
 */
@Service
@Transactional
public class MySHATaskProjectService extends TaskProjectService {

    private final Logger log = LoggerFactory.getLogger(MySHATaskProjectService.class);

    private final MySHATaskProjectRepository mySHATaskProjectRepository;

    public MySHATaskProjectService(TaskProjectRepository taskProjectRepository, TaskProjectMapper taskProjectMapper,
            MySHATaskProjectRepository mySHATaskProjectRepository) {
        super(taskProjectRepository, taskProjectMapper);
        this.mySHATaskProjectRepository = mySHATaskProjectRepository;
    }

    /**
     * Get all the taskProjects owned by a given owner.
     *
     * @param ownerId the id of the owner
     * @return the list of entities.
     */
    public Page<TaskProjectDTO> findAllWhereOwnerId(Pageable pageable, Long ownerId) {
        return mySHATaskProjectRepository.findAllWhereOwnerId(pageable, ownerId).map(taskProjectMapper::toDto);
    }

   
}
