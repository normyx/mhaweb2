package com.mgoulene.mhaweb.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.mgoulene.mhaweb.domain.ProfilData;
import com.mgoulene.mhaweb.domain.*; // for static metamodels
import com.mgoulene.mhaweb.repository.ProfilDataRepository;
import com.mgoulene.mhaweb.service.dto.ProfilDataCriteria;
import com.mgoulene.mhaweb.service.dto.ProfilDataDTO;
import com.mgoulene.mhaweb.service.mapper.ProfilDataMapper;

/**
 * Service for executing complex queries for {@link ProfilData} entities in the database.
 * The main input is a {@link ProfilDataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProfilDataDTO} or a {@link Page} of {@link ProfilDataDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProfilDataQueryService extends QueryService<ProfilData> {

    private final Logger log = LoggerFactory.getLogger(ProfilDataQueryService.class);

    private final ProfilDataRepository profilDataRepository;

    private final ProfilDataMapper profilDataMapper;

    public ProfilDataQueryService(ProfilDataRepository profilDataRepository, ProfilDataMapper profilDataMapper) {
        this.profilDataRepository = profilDataRepository;
        this.profilDataMapper = profilDataMapper;
    }

    /**
     * Return a {@link List} of {@link ProfilDataDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProfilDataDTO> findByCriteria(ProfilDataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProfilData> specification = createSpecification(criteria);
        return profilDataMapper.toDto(profilDataRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProfilDataDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProfilDataDTO> findByCriteria(ProfilDataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProfilData> specification = createSpecification(criteria);
        return profilDataRepository.findAll(specification, page)
            .map(profilDataMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProfilDataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProfilData> specification = createSpecification(criteria);
        return profilDataRepository.count(specification);
    }

    /**
     * Function to convert {@link ProfilDataCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ProfilData> createSpecification(ProfilDataCriteria criteria) {
        Specification<ProfilData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ProfilData_.id));
            }
            if (criteria.getLastUpdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdate(), ProfilData_.lastUpdate));
            }
        }
        return specification;
    }
}
