package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.service.TodoListService;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;
import com.mgoulene.mhaweb.service.dto.TodoListDTO;
import com.mgoulene.mhaweb.service.dto.TodoListCriteria;
import com.mgoulene.mhaweb.service.TodoListQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.mhaweb.domain.TodoList}.
 */
@RestController
@RequestMapping("/api")
public class TodoListResource {

    private final Logger log = LoggerFactory.getLogger(TodoListResource.class);

    private static final String ENTITY_NAME = "todoList";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TodoListService todoListService;

    private final TodoListQueryService todoListQueryService;

    public TodoListResource(TodoListService todoListService, TodoListQueryService todoListQueryService) {
        this.todoListService = todoListService;
        this.todoListQueryService = todoListQueryService;
    }

    /**
     * {@code POST  /todo-lists} : Create a new todoList.
     *
     * @param todoListDTO the todoListDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new todoListDTO, or with status {@code 400 (Bad Request)} if the todoList has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/todo-lists")
    public ResponseEntity<TodoListDTO> createTodoList(@Valid @RequestBody TodoListDTO todoListDTO) throws URISyntaxException {
        log.debug("REST request to save TodoList : {}", todoListDTO);
        if (todoListDTO.getId() != null) {
            throw new BadRequestAlertException("A new todoList cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TodoListDTO result = todoListService.save(todoListDTO);
        return ResponseEntity.created(new URI("/api/todo-lists/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /todo-lists} : Updates an existing todoList.
     *
     * @param todoListDTO the todoListDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated todoListDTO,
     * or with status {@code 400 (Bad Request)} if the todoListDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the todoListDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/todo-lists")
    public ResponseEntity<TodoListDTO> updateTodoList(@Valid @RequestBody TodoListDTO todoListDTO) throws URISyntaxException {
        log.debug("REST request to update TodoList : {}", todoListDTO);
        if (todoListDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TodoListDTO result = todoListService.save(todoListDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, todoListDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /todo-lists} : get all the todoLists.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of todoLists in body.
     */
    @GetMapping("/todo-lists")
    public ResponseEntity<List<TodoListDTO>> getAllTodoLists(TodoListCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TodoLists by criteria: {}", criteria);
        Page<TodoListDTO> page = todoListQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /todo-lists/count} : count all the todoLists.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/todo-lists/count")
    public ResponseEntity<Long> countTodoLists(TodoListCriteria criteria) {
        log.debug("REST request to count TodoLists by criteria: {}", criteria);
        return ResponseEntity.ok().body(todoListQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /todo-lists/:id} : get the "id" todoList.
     *
     * @param id the id of the todoListDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the todoListDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/todo-lists/{id}")
    public ResponseEntity<TodoListDTO> getTodoList(@PathVariable Long id) {
        log.debug("REST request to get TodoList : {}", id);
        Optional<TodoListDTO> todoListDTO = todoListService.findOne(id);
        return ResponseUtil.wrapOrNotFound(todoListDTO);
    }

    /**
     * {@code DELETE  /todo-lists/:id} : delete the "id" todoList.
     *
     * @param id the id of the todoListDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/todo-lists/{id}")
    public ResponseEntity<Void> deleteTodoList(@PathVariable Long id) {
        log.debug("REST request to delete TodoList : {}", id);
        todoListService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
