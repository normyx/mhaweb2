package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.service.SpentService;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;
import com.mgoulene.mhaweb.service.dto.SpentDTO;
import com.mgoulene.mhaweb.service.dto.SpentCriteria;
import com.mgoulene.mhaweb.service.SpentQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.mhaweb.domain.Spent}.
 */
@RestController
@RequestMapping("/api")
public class SpentResource {

    private final Logger log = LoggerFactory.getLogger(SpentResource.class);

    private static final String ENTITY_NAME = "spent";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SpentService spentService;

    private final SpentQueryService spentQueryService;

    public SpentResource(SpentService spentService, SpentQueryService spentQueryService) {
        this.spentService = spentService;
        this.spentQueryService = spentQueryService;
    }

    /**
     * {@code POST  /spents} : Create a new spent.
     *
     * @param spentDTO the spentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new spentDTO, or with status {@code 400 (Bad Request)} if the spent has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/spents")
    public ResponseEntity<SpentDTO> createSpent(@Valid @RequestBody SpentDTO spentDTO) throws URISyntaxException {
        log.debug("REST request to save Spent : {}", spentDTO);
        if (spentDTO.getId() != null) {
            throw new BadRequestAlertException("A new spent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SpentDTO result = spentService.save(spentDTO);
        return ResponseEntity.created(new URI("/api/spents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /spents} : Updates an existing spent.
     *
     * @param spentDTO the spentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated spentDTO,
     * or with status {@code 400 (Bad Request)} if the spentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the spentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/spents")
    public ResponseEntity<SpentDTO> updateSpent(@Valid @RequestBody SpentDTO spentDTO) throws URISyntaxException {
        log.debug("REST request to update Spent : {}", spentDTO);
        if (spentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SpentDTO result = spentService.save(spentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, spentDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /spents} : get all the spents.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of spents in body.
     */
    @GetMapping("/spents")
    public ResponseEntity<List<SpentDTO>> getAllSpents(SpentCriteria criteria, Pageable pageable) {
        log.debug("REST request to get Spents by criteria: {}", criteria);
        Page<SpentDTO> page = spentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /spents/count} : count all the spents.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/spents/count")
    public ResponseEntity<Long> countSpents(SpentCriteria criteria) {
        log.debug("REST request to count Spents by criteria: {}", criteria);
        return ResponseEntity.ok().body(spentQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /spents/:id} : get the "id" spent.
     *
     * @param id the id of the spentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the spentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/spents/{id}")
    public ResponseEntity<SpentDTO> getSpent(@PathVariable Long id) {
        log.debug("REST request to get Spent : {}", id);
        Optional<SpentDTO> spentDTO = spentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(spentDTO);
    }

    /**
     * {@code DELETE  /spents/:id} : delete the "id" spent.
     *
     * @param id the id of the spentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/spents/{id}")
    public ResponseEntity<Void> deleteSpent(@PathVariable Long id) {
        log.debug("REST request to delete Spent : {}", id);
        spentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
