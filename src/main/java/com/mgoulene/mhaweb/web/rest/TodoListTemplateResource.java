package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.service.TodoListTemplateService;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;
import com.mgoulene.mhaweb.service.dto.TodoListTemplateDTO;
import com.mgoulene.mhaweb.service.dto.TodoListTemplateCriteria;
import com.mgoulene.mhaweb.service.TodoListTemplateQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.mhaweb.domain.TodoListTemplate}.
 */
@RestController
@RequestMapping("/api")
public class TodoListTemplateResource {

    private final Logger log = LoggerFactory.getLogger(TodoListTemplateResource.class);

    private static final String ENTITY_NAME = "todoListTemplate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TodoListTemplateService todoListTemplateService;

    private final TodoListTemplateQueryService todoListTemplateQueryService;

    public TodoListTemplateResource(TodoListTemplateService todoListTemplateService, TodoListTemplateQueryService todoListTemplateQueryService) {
        this.todoListTemplateService = todoListTemplateService;
        this.todoListTemplateQueryService = todoListTemplateQueryService;
    }

    /**
     * {@code POST  /todo-list-templates} : Create a new todoListTemplate.
     *
     * @param todoListTemplateDTO the todoListTemplateDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new todoListTemplateDTO, or with status {@code 400 (Bad Request)} if the todoListTemplate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/todo-list-templates")
    public ResponseEntity<TodoListTemplateDTO> createTodoListTemplate(@Valid @RequestBody TodoListTemplateDTO todoListTemplateDTO) throws URISyntaxException {
        log.debug("REST request to save TodoListTemplate : {}", todoListTemplateDTO);
        if (todoListTemplateDTO.getId() != null) {
            throw new BadRequestAlertException("A new todoListTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TodoListTemplateDTO result = todoListTemplateService.save(todoListTemplateDTO);
        return ResponseEntity.created(new URI("/api/todo-list-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /todo-list-templates} : Updates an existing todoListTemplate.
     *
     * @param todoListTemplateDTO the todoListTemplateDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated todoListTemplateDTO,
     * or with status {@code 400 (Bad Request)} if the todoListTemplateDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the todoListTemplateDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/todo-list-templates")
    public ResponseEntity<TodoListTemplateDTO> updateTodoListTemplate(@Valid @RequestBody TodoListTemplateDTO todoListTemplateDTO) throws URISyntaxException {
        log.debug("REST request to update TodoListTemplate : {}", todoListTemplateDTO);
        if (todoListTemplateDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TodoListTemplateDTO result = todoListTemplateService.save(todoListTemplateDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, todoListTemplateDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /todo-list-templates} : get all the todoListTemplates.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of todoListTemplates in body.
     */
    @GetMapping("/todo-list-templates")
    public ResponseEntity<List<TodoListTemplateDTO>> getAllTodoListTemplates(TodoListTemplateCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TodoListTemplates by criteria: {}", criteria);
        Page<TodoListTemplateDTO> page = todoListTemplateQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /todo-list-templates/count} : count all the todoListTemplates.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/todo-list-templates/count")
    public ResponseEntity<Long> countTodoListTemplates(TodoListTemplateCriteria criteria) {
        log.debug("REST request to count TodoListTemplates by criteria: {}", criteria);
        return ResponseEntity.ok().body(todoListTemplateQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /todo-list-templates/:id} : get the "id" todoListTemplate.
     *
     * @param id the id of the todoListTemplateDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the todoListTemplateDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/todo-list-templates/{id}")
    public ResponseEntity<TodoListTemplateDTO> getTodoListTemplate(@PathVariable Long id) {
        log.debug("REST request to get TodoListTemplate : {}", id);
        Optional<TodoListTemplateDTO> todoListTemplateDTO = todoListTemplateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(todoListTemplateDTO);
    }

    /**
     * {@code DELETE  /todo-list-templates/:id} : delete the "id" todoListTemplate.
     *
     * @param id the id of the todoListTemplateDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/todo-list-templates/{id}")
    public ResponseEntity<Void> deleteTodoListTemplate(@PathVariable Long id) {
        log.debug("REST request to delete TodoListTemplate : {}", id);
        todoListTemplateService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
