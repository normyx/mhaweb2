/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mgoulene.mhaweb.web.rest.vm;
