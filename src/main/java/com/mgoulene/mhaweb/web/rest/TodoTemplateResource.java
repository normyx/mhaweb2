package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.service.TodoTemplateService;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;
import com.mgoulene.mhaweb.service.dto.TodoTemplateDTO;
import com.mgoulene.mhaweb.service.dto.TodoTemplateCriteria;
import com.mgoulene.mhaweb.service.TodoTemplateQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.mhaweb.domain.TodoTemplate}.
 */
@RestController
@RequestMapping("/api")
public class TodoTemplateResource {

    private final Logger log = LoggerFactory.getLogger(TodoTemplateResource.class);

    private static final String ENTITY_NAME = "todoTemplate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TodoTemplateService todoTemplateService;

    private final TodoTemplateQueryService todoTemplateQueryService;

    public TodoTemplateResource(TodoTemplateService todoTemplateService, TodoTemplateQueryService todoTemplateQueryService) {
        this.todoTemplateService = todoTemplateService;
        this.todoTemplateQueryService = todoTemplateQueryService;
    }

    /**
     * {@code POST  /todo-templates} : Create a new todoTemplate.
     *
     * @param todoTemplateDTO the todoTemplateDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new todoTemplateDTO, or with status {@code 400 (Bad Request)} if the todoTemplate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/todo-templates")
    public ResponseEntity<TodoTemplateDTO> createTodoTemplate(@Valid @RequestBody TodoTemplateDTO todoTemplateDTO) throws URISyntaxException {
        log.debug("REST request to save TodoTemplate : {}", todoTemplateDTO);
        if (todoTemplateDTO.getId() != null) {
            throw new BadRequestAlertException("A new todoTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TodoTemplateDTO result = todoTemplateService.save(todoTemplateDTO);
        return ResponseEntity.created(new URI("/api/todo-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /todo-templates} : Updates an existing todoTemplate.
     *
     * @param todoTemplateDTO the todoTemplateDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated todoTemplateDTO,
     * or with status {@code 400 (Bad Request)} if the todoTemplateDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the todoTemplateDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/todo-templates")
    public ResponseEntity<TodoTemplateDTO> updateTodoTemplate(@Valid @RequestBody TodoTemplateDTO todoTemplateDTO) throws URISyntaxException {
        log.debug("REST request to update TodoTemplate : {}", todoTemplateDTO);
        if (todoTemplateDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TodoTemplateDTO result = todoTemplateService.save(todoTemplateDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, todoTemplateDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /todo-templates} : get all the todoTemplates.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of todoTemplates in body.
     */
    @GetMapping("/todo-templates")
    public ResponseEntity<List<TodoTemplateDTO>> getAllTodoTemplates(TodoTemplateCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TodoTemplates by criteria: {}", criteria);
        Page<TodoTemplateDTO> page = todoTemplateQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /todo-templates/count} : count all the todoTemplates.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/todo-templates/count")
    public ResponseEntity<Long> countTodoTemplates(TodoTemplateCriteria criteria) {
        log.debug("REST request to count TodoTemplates by criteria: {}", criteria);
        return ResponseEntity.ok().body(todoTemplateQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /todo-templates/:id} : get the "id" todoTemplate.
     *
     * @param id the id of the todoTemplateDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the todoTemplateDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/todo-templates/{id}")
    public ResponseEntity<TodoTemplateDTO> getTodoTemplate(@PathVariable Long id) {
        log.debug("REST request to get TodoTemplate : {}", id);
        Optional<TodoTemplateDTO> todoTemplateDTO = todoTemplateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(todoTemplateDTO);
    }

    /**
     * {@code DELETE  /todo-templates/:id} : delete the "id" todoTemplate.
     *
     * @param id the id of the todoTemplateDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/todo-templates/{id}")
    public ResponseEntity<Void> deleteTodoTemplate(@PathVariable Long id) {
        log.debug("REST request to delete TodoTemplate : {}", id);
        todoTemplateService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
