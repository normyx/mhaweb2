package com.mgoulene.mhaweb.web.rest;

import java.time.LocalDate;
import java.util.List;

import com.mgoulene.mhaweb.service.MySHAProfilDataService;
import com.mgoulene.mhaweb.service.dto.ProfilDataDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing {@link com.mgoulene.msha.domain.Task}.
 */
@RestController
@RequestMapping("/api")
public class MySHAProfilDataResource {

    private final Logger log = LoggerFactory.getLogger(MySHAProfilDataResource.class);

    private static final String ENTITY_NAME = "profilData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MySHAProfilDataService mySHAProfilDataService;

    

    public MySHAProfilDataResource(MySHAProfilDataService mySHAProfilDataService) {
        this.mySHAProfilDataService = mySHAProfilDataService;
        
    }


    
    @GetMapping("/mysha-profil-data-where-workspace/{workspaceIds}")
    public ResponseEntity<List<ProfilDataDTO>> getProfilData(@PathVariable(name = "workspaceIds", required = true) List<Long> workspaceIds) {
        log.debug("REST request to get ProfilData from workspaceId: {}", workspaceIds);
        return ResponseEntity.ok().body(mySHAProfilDataService.findAllWhereWorkspace(workspaceIds, null));
    }

    @GetMapping("/mysha-profil-data-where-workspace-and-last-update/{workspaceIds}/{refreshDate}")
    public ResponseEntity<List<ProfilDataDTO>> getProfilData(@PathVariable("workspaceIds") List<Long> workspaceIds, @PathVariable("refreshDate") LocalDate refreshDate) {
        log.debug("REST request to get ProfilData from workspaceId: {} {}", workspaceIds, refreshDate);
        return ResponseEntity.ok().body(mySHAProfilDataService.findAllWhereWorkspace(workspaceIds, refreshDate));
    }
}
