package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.service.TaskProjectService;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;
import com.mgoulene.mhaweb.service.dto.TaskProjectDTO;
import com.mgoulene.mhaweb.service.dto.TaskProjectCriteria;
import com.mgoulene.mhaweb.service.TaskProjectQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.mhaweb.domain.TaskProject}.
 */
@RestController
@RequestMapping("/api")
public class TaskProjectResource {

    private final Logger log = LoggerFactory.getLogger(TaskProjectResource.class);

    private static final String ENTITY_NAME = "taskProject";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TaskProjectService taskProjectService;

    private final TaskProjectQueryService taskProjectQueryService;

    public TaskProjectResource(TaskProjectService taskProjectService, TaskProjectQueryService taskProjectQueryService) {
        this.taskProjectService = taskProjectService;
        this.taskProjectQueryService = taskProjectQueryService;
    }

    /**
     * {@code POST  /task-projects} : Create a new taskProject.
     *
     * @param taskProjectDTO the taskProjectDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new taskProjectDTO, or with status {@code 400 (Bad Request)} if the taskProject has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/task-projects")
    public ResponseEntity<TaskProjectDTO> createTaskProject(@Valid @RequestBody TaskProjectDTO taskProjectDTO) throws URISyntaxException {
        log.debug("REST request to save TaskProject : {}", taskProjectDTO);
        if (taskProjectDTO.getId() != null) {
            throw new BadRequestAlertException("A new taskProject cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TaskProjectDTO result = taskProjectService.save(taskProjectDTO);
        return ResponseEntity.created(new URI("/api/task-projects/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /task-projects} : Updates an existing taskProject.
     *
     * @param taskProjectDTO the taskProjectDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated taskProjectDTO,
     * or with status {@code 400 (Bad Request)} if the taskProjectDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the taskProjectDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/task-projects")
    public ResponseEntity<TaskProjectDTO> updateTaskProject(@Valid @RequestBody TaskProjectDTO taskProjectDTO) throws URISyntaxException {
        log.debug("REST request to update TaskProject : {}", taskProjectDTO);
        if (taskProjectDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TaskProjectDTO result = taskProjectService.save(taskProjectDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, taskProjectDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /task-projects} : get all the taskProjects.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of taskProjects in body.
     */
    @GetMapping("/task-projects")
    public ResponseEntity<List<TaskProjectDTO>> getAllTaskProjects(TaskProjectCriteria criteria, Pageable pageable) {
        log.debug("REST request to get TaskProjects by criteria: {}", criteria);
        Page<TaskProjectDTO> page = taskProjectQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /task-projects/count} : count all the taskProjects.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/task-projects/count")
    public ResponseEntity<Long> countTaskProjects(TaskProjectCriteria criteria) {
        log.debug("REST request to count TaskProjects by criteria: {}", criteria);
        return ResponseEntity.ok().body(taskProjectQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /task-projects/:id} : get the "id" taskProject.
     *
     * @param id the id of the taskProjectDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the taskProjectDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/task-projects/{id}")
    public ResponseEntity<TaskProjectDTO> getTaskProject(@PathVariable Long id) {
        log.debug("REST request to get TaskProject : {}", id);
        Optional<TaskProjectDTO> taskProjectDTO = taskProjectService.findOne(id);
        return ResponseUtil.wrapOrNotFound(taskProjectDTO);
    }

    /**
     * {@code DELETE  /task-projects/:id} : delete the "id" taskProject.
     *
     * @param id the id of the taskProjectDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/task-projects/{id}")
    public ResponseEntity<Void> deleteTaskProject(@PathVariable Long id) {
        log.debug("REST request to delete TaskProject : {}", id);
        taskProjectService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
