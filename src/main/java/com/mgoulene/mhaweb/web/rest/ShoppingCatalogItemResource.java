package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.service.ShoppingCatalogItemService;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogItemDTO;
import com.mgoulene.mhaweb.service.dto.ShoppingCatalogItemCriteria;
import com.mgoulene.mhaweb.service.ShoppingCatalogItemQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.mhaweb.domain.ShoppingCatalogItem}.
 */
@RestController
@RequestMapping("/api")
public class ShoppingCatalogItemResource {

    private final Logger log = LoggerFactory.getLogger(ShoppingCatalogItemResource.class);

    private static final String ENTITY_NAME = "shoppingCatalogItem";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShoppingCatalogItemService shoppingCatalogItemService;

    private final ShoppingCatalogItemQueryService shoppingCatalogItemQueryService;

    public ShoppingCatalogItemResource(ShoppingCatalogItemService shoppingCatalogItemService, ShoppingCatalogItemQueryService shoppingCatalogItemQueryService) {
        this.shoppingCatalogItemService = shoppingCatalogItemService;
        this.shoppingCatalogItemQueryService = shoppingCatalogItemQueryService;
    }

    /**
     * {@code POST  /shopping-catalog-items} : Create a new shoppingCatalogItem.
     *
     * @param shoppingCatalogItemDTO the shoppingCatalogItemDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shoppingCatalogItemDTO, or with status {@code 400 (Bad Request)} if the shoppingCatalogItem has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shopping-catalog-items")
    public ResponseEntity<ShoppingCatalogItemDTO> createShoppingCatalogItem(@Valid @RequestBody ShoppingCatalogItemDTO shoppingCatalogItemDTO) throws URISyntaxException {
        log.debug("REST request to save ShoppingCatalogItem : {}", shoppingCatalogItemDTO);
        if (shoppingCatalogItemDTO.getId() != null) {
            throw new BadRequestAlertException("A new shoppingCatalogItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShoppingCatalogItemDTO result = shoppingCatalogItemService.save(shoppingCatalogItemDTO);
        return ResponseEntity.created(new URI("/api/shopping-catalog-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shopping-catalog-items} : Updates an existing shoppingCatalogItem.
     *
     * @param shoppingCatalogItemDTO the shoppingCatalogItemDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shoppingCatalogItemDTO,
     * or with status {@code 400 (Bad Request)} if the shoppingCatalogItemDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shoppingCatalogItemDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shopping-catalog-items")
    public ResponseEntity<ShoppingCatalogItemDTO> updateShoppingCatalogItem(@Valid @RequestBody ShoppingCatalogItemDTO shoppingCatalogItemDTO) throws URISyntaxException {
        log.debug("REST request to update ShoppingCatalogItem : {}", shoppingCatalogItemDTO);
        if (shoppingCatalogItemDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ShoppingCatalogItemDTO result = shoppingCatalogItemService.save(shoppingCatalogItemDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shoppingCatalogItemDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shopping-catalog-items} : get all the shoppingCatalogItems.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shoppingCatalogItems in body.
     */
    @GetMapping("/shopping-catalog-items")
    public ResponseEntity<List<ShoppingCatalogItemDTO>> getAllShoppingCatalogItems(ShoppingCatalogItemCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ShoppingCatalogItems by criteria: {}", criteria);
        Page<ShoppingCatalogItemDTO> page = shoppingCatalogItemQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shopping-catalog-items/count} : count all the shoppingCatalogItems.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/shopping-catalog-items/count")
    public ResponseEntity<Long> countShoppingCatalogItems(ShoppingCatalogItemCriteria criteria) {
        log.debug("REST request to count ShoppingCatalogItems by criteria: {}", criteria);
        return ResponseEntity.ok().body(shoppingCatalogItemQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /shopping-catalog-items/:id} : get the "id" shoppingCatalogItem.
     *
     * @param id the id of the shoppingCatalogItemDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shoppingCatalogItemDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shopping-catalog-items/{id}")
    public ResponseEntity<ShoppingCatalogItemDTO> getShoppingCatalogItem(@PathVariable Long id) {
        log.debug("REST request to get ShoppingCatalogItem : {}", id);
        Optional<ShoppingCatalogItemDTO> shoppingCatalogItemDTO = shoppingCatalogItemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shoppingCatalogItemDTO);
    }

    /**
     * {@code DELETE  /shopping-catalog-items/:id} : delete the "id" shoppingCatalogItem.
     *
     * @param id the id of the shoppingCatalogItemDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shopping-catalog-items/{id}")
    public ResponseEntity<Void> deleteShoppingCatalogItem(@PathVariable Long id) {
        log.debug("REST request to delete ShoppingCatalogItem : {}", id);
        shoppingCatalogItemService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
