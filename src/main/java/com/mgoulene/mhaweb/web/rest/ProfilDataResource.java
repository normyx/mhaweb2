package com.mgoulene.mhaweb.web.rest;

import com.mgoulene.mhaweb.service.ProfilDataService;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;
import com.mgoulene.mhaweb.service.dto.ProfilDataDTO;
import com.mgoulene.mhaweb.service.dto.ProfilDataCriteria;
import com.mgoulene.mhaweb.service.ProfilDataQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mgoulene.mhaweb.domain.ProfilData}.
 */
@RestController
@RequestMapping("/api")
public class ProfilDataResource {

    private final Logger log = LoggerFactory.getLogger(ProfilDataResource.class);

    private static final String ENTITY_NAME = "profilData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProfilDataService profilDataService;

    private final ProfilDataQueryService profilDataQueryService;

    public ProfilDataResource(ProfilDataService profilDataService, ProfilDataQueryService profilDataQueryService) {
        this.profilDataService = profilDataService;
        this.profilDataQueryService = profilDataQueryService;
    }

    /**
     * {@code POST  /profil-data} : Create a new profilData.
     *
     * @param profilDataDTO the profilDataDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new profilDataDTO, or with status {@code 400 (Bad Request)} if the profilData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/profil-data")
    public ResponseEntity<ProfilDataDTO> createProfilData(@Valid @RequestBody ProfilDataDTO profilDataDTO) throws URISyntaxException {
        log.debug("REST request to save ProfilData : {}", profilDataDTO);
        if (profilDataDTO.getId() != null) {
            throw new BadRequestAlertException("A new profilData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProfilDataDTO result = profilDataService.save(profilDataDTO);
        return ResponseEntity.created(new URI("/api/profil-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /profil-data} : Updates an existing profilData.
     *
     * @param profilDataDTO the profilDataDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated profilDataDTO,
     * or with status {@code 400 (Bad Request)} if the profilDataDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the profilDataDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/profil-data")
    public ResponseEntity<ProfilDataDTO> updateProfilData(@Valid @RequestBody ProfilDataDTO profilDataDTO) throws URISyntaxException {
        log.debug("REST request to update ProfilData : {}", profilDataDTO);
        if (profilDataDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProfilDataDTO result = profilDataService.save(profilDataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, profilDataDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /profil-data} : get all the profilData.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of profilData in body.
     */
    @GetMapping("/profil-data")
    public ResponseEntity<List<ProfilDataDTO>> getAllProfilData(ProfilDataCriteria criteria) {
        log.debug("REST request to get ProfilData by criteria: {}", criteria);
        List<ProfilDataDTO> entityList = profilDataQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
     * {@code GET  /profil-data/count} : count all the profilData.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/profil-data/count")
    public ResponseEntity<Long> countProfilData(ProfilDataCriteria criteria) {
        log.debug("REST request to count ProfilData by criteria: {}", criteria);
        return ResponseEntity.ok().body(profilDataQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /profil-data/:id} : get the "id" profilData.
     *
     * @param id the id of the profilDataDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the profilDataDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/profil-data/{id}")
    public ResponseEntity<ProfilDataDTO> getProfilData(@PathVariable Long id) {
        log.debug("REST request to get ProfilData : {}", id);
        Optional<ProfilDataDTO> profilDataDTO = profilDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(profilDataDTO);
    }

    /**
     * {@code DELETE  /profil-data/:id} : delete the "id" profilData.
     *
     * @param id the id of the profilDataDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/profil-data/{id}")
    public ResponseEntity<Void> deleteProfilData(@PathVariable Long id) {
        log.debug("REST request to delete ProfilData : {}", id);
        profilDataService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
