package com.mgoulene.mhaweb.web.rest;

import java.util.List;
import java.util.Optional;

import com.mgoulene.mhaweb.domain.User;
import com.mgoulene.mhaweb.service.ProfilQueryService;
import com.mgoulene.mhaweb.service.UserService;
import com.mgoulene.mhaweb.service.dto.ProfilCriteria;
import com.mgoulene.mhaweb.service.dto.ProfilDTO;
import com.mgoulene.mhaweb.service.dto.WorkspaceDTO;
import com.mgoulene.mhaweb.web.rest.errors.BadRequestAlertException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.web.util.PaginationUtil;

/**
 * REST controller for managing {@link com.mgoulene.msha.domain.TaskProject}.
 */
@RestController
@RequestMapping("/api")
public class MySHAProfilResource {

    private final Logger log = LoggerFactory.getLogger(MySHAProfilResource.class);

    private static final String ENTITY_NAME = "profil";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserService userService;
    private final ProfilQueryService profilQueryService;


    public MySHAProfilResource(UserService userService, ProfilQueryService profilQueryService) {
        this.userService = userService;
        this.profilQueryService = profilQueryService;
    }



    /**
     * {@code GET  /task-projects} : get all the taskProjects owned by the looged user.
     *
     * 
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of taskProjects in body.
     */
    @GetMapping("/mysha-profil-where-logged-user")
    public ResponseEntity<ProfilDTO> getAllProfilWhereLoggedUser() {
        log.debug("REST request to get Profil from the logged user");
        Optional<User> userOptional = userService.getUserWithAuthorities();
        if (userOptional.isPresent()) {
            ProfilCriteria pc = new ProfilCriteria();
            LongFilter pclf = new LongFilter();
            pclf.setEquals(userOptional.get().getId());
            pc.setUserId(pclf);
            List<ProfilDTO> profils = profilQueryService.findByCriteria(pc);
            if (profils != null && profils.size() == 1) {
                return ResponseEntity.ok().body(profils.get(0));
            }
            throw new BadRequestAlertException("Cannot retrieve correct Profil", ENTITY_NAME, "profils not got for user : "+profils.toString());
            
        } 
        throw new BadRequestAlertException("Cannot find Workspaces without a logged user", ENTITY_NAME, "idnull");
    }
}
