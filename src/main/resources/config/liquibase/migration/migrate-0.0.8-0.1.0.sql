-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml
-- Ran at: 03/05/20 18:36
-- Against: mha@jdbc:postgresql://192.168.0.71:5432/mysharedhomeactivities
-- Liquibase version: 3.6.3
-- *********************************************************************

-- Lock Database
UPDATE databasechangeloglock SET LOCKED = TRUE, LOCKEDBY = 'CPX-IGHR3VJOV0A (192.168.0.22)', LOCKGRANTED = '2020-05-03 18:36:23.089' WHERE ID = 1 AND LOCKED = FALSE;

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-2::mathieu.goulene (generated)
CREATE TABLE todo (id BIGINT NOT NULL, label VARCHAR(40) NOT NULL, done BOOLEAN NOT NULL, last_update date NOT NULL, todo_list_id BIGINT NOT NULL, CONSTRAINT todo_pkey PRIMARY KEY (id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-2', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 39, '8:65a27552ff5dd5d98ec829fed1f95abf', 'createTable tableName=todo', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-3::mathieu.goulene (generated)
CREATE TABLE todo_list (id BIGINT NOT NULL, label VARCHAR(40) NOT NULL, last_update date NOT NULL, workspace_id BIGINT NOT NULL, CONSTRAINT todo_list_pkey PRIMARY KEY (id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-3', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 40, '8:df5541d57e7dcfed5cafe02c4e2a1ca7', 'createTable tableName=todo_list', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-4::mathieu.goulene (generated)
CREATE TABLE todo_list_owner (owner_id BIGINT NOT NULL, todo_list_id BIGINT NOT NULL, CONSTRAINT todo_list_owner_pkey PRIMARY KEY (owner_id, todo_list_id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-4', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 41, '8:b8ae96f6fc8119abb5399f4abf6718d6', 'createTable tableName=todo_list_owner', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-5::mathieu.goulene (generated)
ALTER TABLE profil ADD last_update date NOT NULL DEFAULT '2020-05-01';

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-5', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 42, '8:c9e252d629c70bf74c54eb2fec062643', 'addColumn tableName=profil', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-6::mathieu.goulene (generated)
ALTER TABLE spent_sharing_config ADD last_update date NOT NULL DEFAULT '2020-05-01';

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-6', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 43, '8:65974a085c8d22be81c7e194dfd3008f', 'addColumn tableName=spent_sharing_config', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-7::mathieu.goulene (generated)
ALTER TABLE task_project ADD last_update date NOT NULL DEFAULT '2020-05-01';

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-7', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 44, '8:73cadf7366836dc6d288785fbb30cc8e', 'addColumn tableName=task_project', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-8::mathieu.goulene (generated)
ALTER TABLE wallet ADD last_update date NOT NULL DEFAULT '2020-05-01';

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-8', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 45, '8:e626f5a079a87e2fef30c6fcf6a93f9b', 'addColumn tableName=wallet', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-9::mathieu.goulene (generated)
ALTER TABLE workspace ADD last_update date NOT NULL DEFAULT '2020-05-01';

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-9', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 46, '8:ea5f627395ab09b45b92cda07049f01e', 'addColumn tableName=workspace', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-10::mathieu.goulene (generated)
ALTER TABLE profil_data ADD last_update date NOT NULL DEFAULT '2020-05-01';

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-10', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 47, '8:1e0d27c3436b95694d8af47a7d6e28b6', 'addColumn tableName=profil_data', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-11::mathieu.goulene (generated)
ALTER TABLE spent_sharing ADD last_update date NOT NULL DEFAULT '2020-05-01';

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-11', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 48, '8:d2b72ab96700f5d149c714638e87b24f', 'addColumn tableName=spent_sharing', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-12::mathieu.goulene (generated)
ALTER TABLE spent_config ADD last_update date NOT NULL DEFAULT '2020-05-01';

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-12', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 49, '8:c24218be42bab0bf37f5e9539fcb3f44', 'addColumn tableName=spent_config', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-13::mathieu.goulene (generated)
ALTER TABLE task ADD last_update date NOT NULL DEFAULT '2020-05-01';

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-13', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 50, '8:3b727ee6d163547ab2225984c7d793d6', 'addColumn tableName=task', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-14::mathieu.goulene (generated)
ALTER TABLE spent ADD last_update date NOT NULL DEFAULT '2020-05-01';

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-14', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 51, '8:df75b94e1c96983ad895a187812f5f7e', 'addColumn tableName=spent', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-15::mathieu.goulene (generated)
ALTER TABLE todo_list_owner ADD CONSTRAINT fk_todo_list_owner_owner_id FOREIGN KEY (owner_id) REFERENCES profil (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-15', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 52, '8:ab2bedf32ed6324c97bd871af8eabea6', 'addForeignKeyConstraint baseTableName=todo_list_owner, constraintName=fk_todo_list_owner_owner_id, referencedTableName=profil', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-16::mathieu.goulene (generated)
ALTER TABLE todo_list_owner ADD CONSTRAINT fk_todo_list_owner_todo_list_id FOREIGN KEY (todo_list_id) REFERENCES todo_list (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-16', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 53, '8:e1e0a8050ae036ab124cdf87f0e43397', 'addForeignKeyConstraint baseTableName=todo_list_owner, constraintName=fk_todo_list_owner_todo_list_id, referencedTableName=todo_list', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-17::mathieu.goulene (generated)
ALTER TABLE todo_list ADD CONSTRAINT fk_todo_list_workspace_id FOREIGN KEY (workspace_id) REFERENCES workspace (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-17', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 54, '8:edde558eb2ca32cb593c1fd0f1d676e2', 'addForeignKeyConstraint baseTableName=todo_list, constraintName=fk_todo_list_workspace_id, referencedTableName=workspace', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-18::mathieu.goulene (generated)
ALTER TABLE todo ADD CONSTRAINT fk_todo_todo_list_id FOREIGN KEY (todo_list_id) REFERENCES todo_list (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-18', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 55, '8:e2c2c503a2646146de87b9b1ff4936f9', 'addForeignKeyConstraint baseTableName=todo, constraintName=fk_todo_todo_list_id, referencedTableName=todo_list', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-19::mathieu.goulene (generated)
ALTER TABLE spent DROP COLUMN last_update_date;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-19', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 56, '8:dfbcf019e4bb31e495673224a105bd02', 'dropColumn columnName=last_update_date, tableName=spent', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml::1588523699546-1::mathieu.goulene (generated)
ALTER TABLE wallet ALTER COLUMN  workspace_id SET NOT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1588523699546-1', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200503163440_changelog.xml', NOW(), 57, '8:0a62fb6604a7fe97cedd74530ffbea4b', 'addNotNullConstraint columnName=workspace_id, tableName=wallet', '', 'EXECUTED', NULL, NULL, '3.6.3', '8523784918');

-- Release Database Lock
UPDATE databasechangeloglock SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

