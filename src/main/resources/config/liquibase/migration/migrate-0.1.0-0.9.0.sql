-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml
-- Ran at: 06/06/20 19:05
-- Against: mha@jdbc:postgresql://192.168.0.71:5432/mysharedhomeactivities
-- Liquibase version: 3.6.3
-- *********************************************************************

-- Lock Database
UPDATE databasechangeloglock SET LOCKED = TRUE, LOCKEDBY = 'CPX-IGHR3VJOV0A (192.168.0.15)', LOCKGRANTED = '2020-06-06 19:05:46.618' WHERE ID = 1 AND LOCKED = FALSE;

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-12::mathieu.goulene (generated)
CREATE TABLE shopping_cart_owner (owner_id BIGINT NOT NULL, shopping_cart_id BIGINT NOT NULL, CONSTRAINT shopping_cart_owner_pkey PRIMARY KEY (owner_id, shopping_cart_id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-12', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 58, '8:a873a5988ef4c99d15d7d1d569cfbab4', 'createTable tableName=shopping_cart_owner', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-13::mathieu.goulene (generated)
CREATE TABLE shopping_catalog_cart (id BIGINT NOT NULL, label VARCHAR(40) NOT NULL, last_update date NOT NULL, CONSTRAINT shopping_catalog_cart_pkey PRIMARY KEY (id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-13', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 59, '8:8627c7f811ca8bbe3fb1de3d56159967', 'createTable tableName=shopping_catalog_cart', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-14::mathieu.goulene (generated)
CREATE TABLE shopping_catalog_item (id BIGINT NOT NULL, label VARCHAR(80) NOT NULL, last_update date NOT NULL, shopping_catalog_sheld_id BIGINT NOT NULL, CONSTRAINT shopping_catalog_item_pkey PRIMARY KEY (id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-14', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 60, '8:d6dc977c68c2a1a285e949ea90b880fc', 'createTable tableName=shopping_catalog_item', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-15::mathieu.goulene (generated)
CREATE TABLE shopping_catalog_sheld (id BIGINT NOT NULL, label VARCHAR(40) NOT NULL, last_update date NOT NULL, is_default BOOLEAN NOT NULL, is_deleted BOOLEAN NOT NULL, shopping_catalog_cart_id BIGINT NOT NULL, CONSTRAINT shopping_catalog_sheld_pkey PRIMARY KEY (id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-15', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 61, '8:8616740e406a8b263f22e40c4e14a353', 'createTable tableName=shopping_catalog_sheld', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-16::mathieu.goulene (generated)
CREATE TABLE shopping_item (id BIGINT NOT NULL, label VARCHAR(80) NOT NULL, last_update date NOT NULL, checked BOOLEAN NOT NULL, quantity FLOAT4, unit VARCHAR(255), shopping_catalog_sheld_id BIGINT NOT NULL, shopping_cart_id BIGINT NOT NULL, CONSTRAINT shopping_item_pkey PRIMARY KEY (id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-16', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 62, '8:5bd1cd520df8282629153b1d730e5b4d', 'createTable tableName=shopping_item', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-17::mathieu.goulene (generated)
CREATE TABLE todo_list_template (id BIGINT NOT NULL, label VARCHAR(40) NOT NULL, last_update date NOT NULL, workspace_id BIGINT NOT NULL, CONSTRAINT todo_list_template_pkey PRIMARY KEY (id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-17', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 63, '8:393885fa352e5d8eb2f26b2b7d68f36c', 'createTable tableName=todo_list_template', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-18::mathieu.goulene (generated)
CREATE TABLE todo_list_template_owner (owner_id BIGINT NOT NULL, todo_list_template_id BIGINT NOT NULL, CONSTRAINT todo_list_template_owner_pkey PRIMARY KEY (owner_id, todo_list_template_id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-18', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 64, '8:0e273578711eb6fbef5ad3cf137e67a2', 'createTable tableName=todo_list_template_owner', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-19::mathieu.goulene (generated)
CREATE TABLE todo_template (id BIGINT NOT NULL, label VARCHAR(40) NOT NULL, last_update date NOT NULL, todo_list_template_id BIGINT NOT NULL, CONSTRAINT todo_template_pkey PRIMARY KEY (id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-19', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 65, '8:5b44505c52b90c0eff58f2441aa414c6', 'createTable tableName=todo_template', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-20::mathieu.goulene (generated)
ALTER TABLE shopping_cart ADD label VARCHAR(40) NOT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-20', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 66, '8:7caf2e378a648028fe1b09194eb9dd92', 'addColumn tableName=shopping_cart', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-21::mathieu.goulene (generated)
ALTER TABLE shopping_cart ADD last_update date NOT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-21', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 67, '8:82f56771c12ed0353a4a113d59e4c069', 'addColumn tableName=shopping_cart', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-22::mathieu.goulene (generated)
ALTER TABLE shopping_cart ADD shopping_catalog_cart_id BIGINT;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-22', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 68, '8:3993ad050fb51f4a780265edee61603a', 'addColumn tableName=shopping_cart', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-23::mathieu.goulene (generated)
ALTER TABLE shopping_cart ADD workspace_id BIGINT NOT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-23', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 69, '8:5a24cfacacd435846a8b7452997aa7e9', 'addColumn tableName=shopping_cart', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-24::mathieu.goulene (generated)
ALTER TABLE shopping_cart_owner ADD CONSTRAINT fk_shopping_cart_owner_owner_id FOREIGN KEY (owner_id) REFERENCES profil (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-24', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 70, '8:e96aa869716e3747a7055b78435bda6f', 'addForeignKeyConstraint baseTableName=shopping_cart_owner, constraintName=fk_shopping_cart_owner_owner_id, referencedTableName=profil', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-25::mathieu.goulene (generated)
ALTER TABLE shopping_cart_owner ADD CONSTRAINT fk_shopping_cart_owner_shopping_cart_id FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-25', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 71, '8:9c8176fc4178ae01bec82ea114ba64b1', 'addForeignKeyConstraint baseTableName=shopping_cart_owner, constraintName=fk_shopping_cart_owner_shopping_cart_id, referencedTableName=shopping_cart', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-26::mathieu.goulene (generated)
ALTER TABLE shopping_cart ADD CONSTRAINT fk_shopping_cart_shopping_catalog_cart_id FOREIGN KEY (shopping_catalog_cart_id) REFERENCES shopping_catalog_cart (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-26', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 72, '8:6c73db1ae661c89b83c3eb37f338348d', 'addForeignKeyConstraint baseTableName=shopping_cart, constraintName=fk_shopping_cart_shopping_catalog_cart_id, referencedTableName=shopping_catalog_cart', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-27::mathieu.goulene (generated)
ALTER TABLE shopping_cart ADD CONSTRAINT fk_shopping_cart_workspace_id FOREIGN KEY (workspace_id) REFERENCES workspace (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-27', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 73, '8:0c46dde27dda1f5f715fde6de6c59c04', 'addForeignKeyConstraint baseTableName=shopping_cart, constraintName=fk_shopping_cart_workspace_id, referencedTableName=workspace', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-28::mathieu.goulene (generated)
ALTER TABLE shopping_catalog_item ADD CONSTRAINT fk_shopping_catalog_item_shopping_catalog_sheld_id FOREIGN KEY (shopping_catalog_sheld_id) REFERENCES shopping_catalog_sheld (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-28', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 74, '8:e47c71a625cbf37b50415ff232934fd3', 'addForeignKeyConstraint baseTableName=shopping_catalog_item, constraintName=fk_shopping_catalog_item_shopping_catalog_sheld_id, referencedTableName=shopping_catalog_sheld', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-29::mathieu.goulene (generated)
ALTER TABLE shopping_catalog_sheld ADD CONSTRAINT fk_shopping_catalog_sheld_shopping_catalog_cart_id FOREIGN KEY (shopping_catalog_cart_id) REFERENCES shopping_catalog_cart (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-29', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 75, '8:651b8563ac04bee81ab721295f6821ea', 'addForeignKeyConstraint baseTableName=shopping_catalog_sheld, constraintName=fk_shopping_catalog_sheld_shopping_catalog_cart_id, referencedTableName=shopping_catalog_cart', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-30::mathieu.goulene (generated)
ALTER TABLE shopping_item ADD CONSTRAINT fk_shopping_item_shopping_cart_id FOREIGN KEY (shopping_cart_id) REFERENCES shopping_cart (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-30', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 76, '8:1ecb2de7105dad2a8e4ad5a64b038ba2', 'addForeignKeyConstraint baseTableName=shopping_item, constraintName=fk_shopping_item_shopping_cart_id, referencedTableName=shopping_cart', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-31::mathieu.goulene (generated)
ALTER TABLE shopping_item ADD CONSTRAINT fk_shopping_item_shopping_catalog_sheld_id FOREIGN KEY (shopping_catalog_sheld_id) REFERENCES shopping_catalog_sheld (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-31', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 77, '8:6e9ce55f2f6944f7efc5aecdc241e135', 'addForeignKeyConstraint baseTableName=shopping_item, constraintName=fk_shopping_item_shopping_catalog_sheld_id, referencedTableName=shopping_catalog_sheld', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-32::mathieu.goulene (generated)
ALTER TABLE todo_list_template_owner ADD CONSTRAINT fk_todo_list_template_owner_owner_id FOREIGN KEY (owner_id) REFERENCES profil (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-32', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 78, '8:af51cc75aaddd48e12c77fd6772c921c', 'addForeignKeyConstraint baseTableName=todo_list_template_owner, constraintName=fk_todo_list_template_owner_owner_id, referencedTableName=profil', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-33::mathieu.goulene (generated)
ALTER TABLE todo_list_template_owner ADD CONSTRAINT fk_todo_list_template_owner_todo_list_template_id FOREIGN KEY (todo_list_template_id) REFERENCES todo_list_template (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-33', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 79, '8:fb79015da4891138d2ed0fa482728427', 'addForeignKeyConstraint baseTableName=todo_list_template_owner, constraintName=fk_todo_list_template_owner_todo_list_template_id, referencedTableName=todo_list_template', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-34::mathieu.goulene (generated)
ALTER TABLE todo_list_template ADD CONSTRAINT fk_todo_list_template_workspace_id FOREIGN KEY (workspace_id) REFERENCES workspace (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-34', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 80, '8:aa417873d04fef8ac91e587ebdda241f', 'addForeignKeyConstraint baseTableName=todo_list_template, constraintName=fk_todo_list_template_workspace_id, referencedTableName=workspace', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-35::mathieu.goulene (generated)
ALTER TABLE todo_template ADD CONSTRAINT fk_todo_template_todo_list_template_id FOREIGN KEY (todo_list_template_id) REFERENCES todo_list_template (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-35', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 81, '8:bd2d7fdb79a20d3f08a2af167fdb57e6', 'addForeignKeyConstraint baseTableName=todo_template, constraintName=fk_todo_template_todo_list_template_id, referencedTableName=todo_list_template', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-1::mathieu.goulene (generated)
ALTER TABLE profil ALTER COLUMN  last_update SET DEFAULT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-1', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 82, '8:644bc5955cf64321a3dc44ab2e7fbaac', 'dropDefaultValue columnName=last_update, tableName=profil', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-2::mathieu.goulene (generated)
ALTER TABLE profil_data ALTER COLUMN  last_update SET DEFAULT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-2', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 83, '8:5a8f274fb2745788d8caeff7f788156a', 'dropDefaultValue columnName=last_update, tableName=profil_data', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-3::mathieu.goulene (generated)
ALTER TABLE spent ALTER COLUMN  last_update SET DEFAULT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-3', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 84, '8:aad361ab8f05288e0295cc9aba653bb1', 'dropDefaultValue columnName=last_update, tableName=spent', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-4::mathieu.goulene (generated)
ALTER TABLE spent_config ALTER COLUMN  last_update SET DEFAULT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-4', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 85, '8:c642d612020ff8f907b2da5564942f2a', 'dropDefaultValue columnName=last_update, tableName=spent_config', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-5::mathieu.goulene (generated)
ALTER TABLE spent_sharing ALTER COLUMN  last_update SET DEFAULT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-5', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 86, '8:62b88e69c9aecc89b0d553e0dca749da', 'dropDefaultValue columnName=last_update, tableName=spent_sharing', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-6::mathieu.goulene (generated)
ALTER TABLE spent_sharing_config ALTER COLUMN  last_update SET DEFAULT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-6', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 87, '8:05f7dfc7365bec9a9728310af531dfe3', 'dropDefaultValue columnName=last_update, tableName=spent_sharing_config', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-7::mathieu.goulene (generated)
ALTER TABLE task ALTER COLUMN  last_update SET DEFAULT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-7', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 88, '8:c3d5b8cc82b04c9ecd346e9eea09c83b', 'dropDefaultValue columnName=last_update, tableName=task', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-8::mathieu.goulene (generated)
ALTER TABLE task_project ALTER COLUMN  last_update SET DEFAULT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-8', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 89, '8:e70deaa38e0270aba36974d386b7847f', 'dropDefaultValue columnName=last_update, tableName=task_project', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-9::mathieu.goulene (generated)
ALTER TABLE wallet ALTER COLUMN  last_update SET DEFAULT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-9', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 90, '8:c318060d69819f16102f527d34296ca5', 'dropDefaultValue columnName=last_update, tableName=wallet', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-10::mathieu.goulene (generated)
ALTER TABLE workspace ALTER COLUMN  last_update SET DEFAULT NULL;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-10', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 91, '8:74f65ee44f21d19f3ad02c31def7e350', 'dropDefaultValue columnName=last_update, tableName=workspace', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml::1591462861033-11::mathieu.goulene (generated)
DO $$ DECLARE constraint_name varchar;
BEGIN
  SELECT tc.CONSTRAINT_NAME into strict constraint_name
    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
    WHERE CONSTRAINT_TYPE = 'PRIMARY KEY'
      AND TABLE_NAME = 'todo_list_owner' AND TABLE_SCHEMA = 'public';
    EXECUTE 'alter table public.todo_list_owner drop constraint ' || constraint_name;
END $$;

ALTER TABLE todo_list_owner ADD CONSTRAINT todo_list_owner_pkey PRIMARY KEY (todo_list_id, owner_id);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1591462861033-11', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200606170023_changelog.xml', NOW(), 92, '8:b8b9cd240012107394e5f905659f38d1', 'dropPrimaryKey tableName=todo_list_owner; addPrimaryKey constraintName=todo_list_owner_pkey, tableName=todo_list_owner', '', 'EXECUTED', NULL, NULL, '3.6.3', '1463148066');

-- Release Database Lock
UPDATE databasechangeloglock SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

