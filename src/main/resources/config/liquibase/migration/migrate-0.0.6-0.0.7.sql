-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200221152644_changelog.xml
-- Ran at: 21/02/20 16:29
-- Against: mha@jdbc:postgresql://192.168.0.71:5432/mysharedhomeactivities
-- Liquibase version: 3.6.3
-- *********************************************************************

-- Lock Database
UPDATE databasechangeloglock SET LOCKED = TRUE, LOCKEDBY = 'CPX-IGHR3VJOV0A (192.168.0.26)', LOCKGRANTED = '2020-02-21 16:29:20.999' WHERE ID = 1 AND LOCKED = FALSE;

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200221152644_changelog.xml::1582298825157-2::mathieu.goulene (generated)
ALTER TABLE task_project ADD simplified BOOLEAN NOT NULL DEFAULT false;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1582298825157-2', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200221152644_changelog.xml', NOW(), 35, '8:cfc749380824b20c74f0353aa480c7f1', 'addColumn tableName=task_project', '', 'EXECUTED', NULL, NULL, '3.6.3', '2298962221');

-- Changeset C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200221152644_changelog.xml::1582298825157-1::mathieu.goulene (generated)
ALTER TABLE spent ALTER COLUMN label TYPE VARCHAR(30) USING (label::VARCHAR(30));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('1582298825157-1', 'mathieu.goulene (generated)', 'C:/00_dev/mha/mhaweb/src/main/resources/config/liquibase/changelog/20200221152644_changelog.xml', NOW(), 36, '8:1633e6a4c3a7858e1b5e29a140374908', 'modifyDataType columnName=label, tableName=spent', '', 'EXECUTED', NULL, NULL, '3.6.3', '2298962221');

-- Release Database Lock
UPDATE databasechangeloglock SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

