package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ShoppingCatalogCartMapperTest {

    private ShoppingCatalogCartMapper shoppingCatalogCartMapper;

    @BeforeEach
    public void setUp() {
        shoppingCatalogCartMapper = new ShoppingCatalogCartMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(shoppingCatalogCartMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(shoppingCatalogCartMapper.fromId(null)).isNull();
    }
}
