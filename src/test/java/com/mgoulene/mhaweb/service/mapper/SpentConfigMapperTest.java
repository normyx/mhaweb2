package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class SpentConfigMapperTest {

    private SpentConfigMapper spentConfigMapper;

    @BeforeEach
    public void setUp() {
        spentConfigMapper = new SpentConfigMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(spentConfigMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(spentConfigMapper.fromId(null)).isNull();
    }
}
