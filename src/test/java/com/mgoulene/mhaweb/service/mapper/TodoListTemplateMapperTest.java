package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TodoListTemplateMapperTest {

    private TodoListTemplateMapper todoListTemplateMapper;

    @BeforeEach
    public void setUp() {
        todoListTemplateMapper = new TodoListTemplateMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(todoListTemplateMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(todoListTemplateMapper.fromId(null)).isNull();
    }
}
