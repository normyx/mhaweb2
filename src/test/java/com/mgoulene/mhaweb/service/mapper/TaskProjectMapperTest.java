package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TaskProjectMapperTest {

    private TaskProjectMapper taskProjectMapper;

    @BeforeEach
    public void setUp() {
        taskProjectMapper = new TaskProjectMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(taskProjectMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(taskProjectMapper.fromId(null)).isNull();
    }
}
