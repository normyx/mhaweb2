package com.mgoulene.mhaweb.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class SpentSharingMapperTest {

    private SpentSharingMapper spentSharingMapper;

    @BeforeEach
    public void setUp() {
        spentSharingMapper = new SpentSharingMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(spentSharingMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(spentSharingMapper.fromId(null)).isNull();
    }
}
