package com.mgoulene.mhaweb.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class SpentDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentDTO.class);
        SpentDTO spentDTO1 = new SpentDTO();
        spentDTO1.setId(1L);
        SpentDTO spentDTO2 = new SpentDTO();
        assertThat(spentDTO1).isNotEqualTo(spentDTO2);
        spentDTO2.setId(spentDTO1.getId());
        assertThat(spentDTO1).isEqualTo(spentDTO2);
        spentDTO2.setId(2L);
        assertThat(spentDTO1).isNotEqualTo(spentDTO2);
        spentDTO1.setId(null);
        assertThat(spentDTO1).isNotEqualTo(spentDTO2);
    }
}
