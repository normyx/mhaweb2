package com.mgoulene.mhaweb.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class ProfilDataDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProfilDataDTO.class);
        ProfilDataDTO profilDataDTO1 = new ProfilDataDTO();
        profilDataDTO1.setId(1L);
        ProfilDataDTO profilDataDTO2 = new ProfilDataDTO();
        assertThat(profilDataDTO1).isNotEqualTo(profilDataDTO2);
        profilDataDTO2.setId(profilDataDTO1.getId());
        assertThat(profilDataDTO1).isEqualTo(profilDataDTO2);
        profilDataDTO2.setId(2L);
        assertThat(profilDataDTO1).isNotEqualTo(profilDataDTO2);
        profilDataDTO1.setId(null);
        assertThat(profilDataDTO1).isNotEqualTo(profilDataDTO2);
    }
}
