package com.mgoulene.mhaweb.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class ShoppingCatalogSheldDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShoppingCatalogSheldDTO.class);
        ShoppingCatalogSheldDTO shoppingCatalogSheldDTO1 = new ShoppingCatalogSheldDTO();
        shoppingCatalogSheldDTO1.setId(1L);
        ShoppingCatalogSheldDTO shoppingCatalogSheldDTO2 = new ShoppingCatalogSheldDTO();
        assertThat(shoppingCatalogSheldDTO1).isNotEqualTo(shoppingCatalogSheldDTO2);
        shoppingCatalogSheldDTO2.setId(shoppingCatalogSheldDTO1.getId());
        assertThat(shoppingCatalogSheldDTO1).isEqualTo(shoppingCatalogSheldDTO2);
        shoppingCatalogSheldDTO2.setId(2L);
        assertThat(shoppingCatalogSheldDTO1).isNotEqualTo(shoppingCatalogSheldDTO2);
        shoppingCatalogSheldDTO1.setId(null);
        assertThat(shoppingCatalogSheldDTO1).isNotEqualTo(shoppingCatalogSheldDTO2);
    }
}
