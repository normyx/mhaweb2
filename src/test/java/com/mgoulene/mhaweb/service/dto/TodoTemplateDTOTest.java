package com.mgoulene.mhaweb.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class TodoTemplateDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TodoTemplateDTO.class);
        TodoTemplateDTO todoTemplateDTO1 = new TodoTemplateDTO();
        todoTemplateDTO1.setId(1L);
        TodoTemplateDTO todoTemplateDTO2 = new TodoTemplateDTO();
        assertThat(todoTemplateDTO1).isNotEqualTo(todoTemplateDTO2);
        todoTemplateDTO2.setId(todoTemplateDTO1.getId());
        assertThat(todoTemplateDTO1).isEqualTo(todoTemplateDTO2);
        todoTemplateDTO2.setId(2L);
        assertThat(todoTemplateDTO1).isNotEqualTo(todoTemplateDTO2);
        todoTemplateDTO1.setId(null);
        assertThat(todoTemplateDTO1).isNotEqualTo(todoTemplateDTO2);
    }
}
