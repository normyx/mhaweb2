package com.mgoulene.mhaweb.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class ShoppingCatalogItemDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShoppingCatalogItemDTO.class);
        ShoppingCatalogItemDTO shoppingCatalogItemDTO1 = new ShoppingCatalogItemDTO();
        shoppingCatalogItemDTO1.setId(1L);
        ShoppingCatalogItemDTO shoppingCatalogItemDTO2 = new ShoppingCatalogItemDTO();
        assertThat(shoppingCatalogItemDTO1).isNotEqualTo(shoppingCatalogItemDTO2);
        shoppingCatalogItemDTO2.setId(shoppingCatalogItemDTO1.getId());
        assertThat(shoppingCatalogItemDTO1).isEqualTo(shoppingCatalogItemDTO2);
        shoppingCatalogItemDTO2.setId(2L);
        assertThat(shoppingCatalogItemDTO1).isNotEqualTo(shoppingCatalogItemDTO2);
        shoppingCatalogItemDTO1.setId(null);
        assertThat(shoppingCatalogItemDTO1).isNotEqualTo(shoppingCatalogItemDTO2);
    }
}
