package com.mgoulene.mhaweb.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class TodoListTemplateDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TodoListTemplateDTO.class);
        TodoListTemplateDTO todoListTemplateDTO1 = new TodoListTemplateDTO();
        todoListTemplateDTO1.setId(1L);
        TodoListTemplateDTO todoListTemplateDTO2 = new TodoListTemplateDTO();
        assertThat(todoListTemplateDTO1).isNotEqualTo(todoListTemplateDTO2);
        todoListTemplateDTO2.setId(todoListTemplateDTO1.getId());
        assertThat(todoListTemplateDTO1).isEqualTo(todoListTemplateDTO2);
        todoListTemplateDTO2.setId(2L);
        assertThat(todoListTemplateDTO1).isNotEqualTo(todoListTemplateDTO2);
        todoListTemplateDTO1.setId(null);
        assertThat(todoListTemplateDTO1).isNotEqualTo(todoListTemplateDTO2);
    }
}
