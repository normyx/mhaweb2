package com.mgoulene.mhaweb.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class SpentSharingConfigDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentSharingConfigDTO.class);
        SpentSharingConfigDTO spentSharingConfigDTO1 = new SpentSharingConfigDTO();
        spentSharingConfigDTO1.setId(1L);
        SpentSharingConfigDTO spentSharingConfigDTO2 = new SpentSharingConfigDTO();
        assertThat(spentSharingConfigDTO1).isNotEqualTo(spentSharingConfigDTO2);
        spentSharingConfigDTO2.setId(spentSharingConfigDTO1.getId());
        assertThat(spentSharingConfigDTO1).isEqualTo(spentSharingConfigDTO2);
        spentSharingConfigDTO2.setId(2L);
        assertThat(spentSharingConfigDTO1).isNotEqualTo(spentSharingConfigDTO2);
        spentSharingConfigDTO1.setId(null);
        assertThat(spentSharingConfigDTO1).isNotEqualTo(spentSharingConfigDTO2);
    }
}
