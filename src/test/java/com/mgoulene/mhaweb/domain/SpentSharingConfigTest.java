package com.mgoulene.mhaweb.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mgoulene.mhaweb.web.rest.TestUtil;

public class SpentSharingConfigTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpentSharingConfig.class);
        SpentSharingConfig spentSharingConfig1 = new SpentSharingConfig();
        spentSharingConfig1.setId(1L);
        SpentSharingConfig spentSharingConfig2 = new SpentSharingConfig();
        spentSharingConfig2.setId(spentSharingConfig1.getId());
        assertThat(spentSharingConfig1).isEqualTo(spentSharingConfig2);
        spentSharingConfig2.setId(2L);
        assertThat(spentSharingConfig1).isNotEqualTo(spentSharingConfig2);
        spentSharingConfig1.setId(null);
        assertThat(spentSharingConfig1).isNotEqualTo(spentSharingConfig2);
    }
}
