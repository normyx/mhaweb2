import { element, by, ElementFinder } from 'protractor';

export class ProfilDataComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('mha-profil-data div table .btn-danger'));
  title = element.all(by.css('mha-profil-data div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class ProfilDataUpdatePage {
  pageTitle = element(by.id('mha-profil-data-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  photoInput = element(by.id('file_photo'));
  lastUpdateInput = element(by.id('field_lastUpdate'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setPhotoInput(photo: string): Promise<void> {
    await this.photoInput.sendKeys(photo);
  }

  async getPhotoInput(): Promise<string> {
    return await this.photoInput.getAttribute('value');
  }

  async setLastUpdateInput(lastUpdate: string): Promise<void> {
    await this.lastUpdateInput.sendKeys(lastUpdate);
  }

  async getLastUpdateInput(): Promise<string> {
    return await this.lastUpdateInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class ProfilDataDeleteDialog {
  private dialogTitle = element(by.id('mha-delete-profilData-heading'));
  private confirmButton = element(by.id('mha-confirm-delete-profilData'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
